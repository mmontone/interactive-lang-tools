;;; ilt-repl.el --- interaction mode for Emacs Lisp  -*- lexical-binding: t -*-

;; Copyright (C) 1994, 2001-2022 Free Software Foundation, Inc.

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Keywords: lisp
;; Version: 0.1
;; Package-Requires: ((ilt "0.1"))

;; This file is part of GNU Emacs.

;; GNU Emacs is free software: you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; GNU Emacs is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with GNU Emacs.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Provides a nice interface to evaluating Emacs Lisp expressions.
;; Input is handled by the comint package, and output is passed
;; through the pretty-printer.

;; To start: M-x ilt-repl.  Type C-h m in the *ilt-repl* buffer for more info.

;;; Code:

(require 'comint)
(require 'pp)
(require 'ilt)

;;; User variables

(defgroup ilt-repl nil
  "Interaction mode for Emacs Lisp."
  :group 'lisp)


(defcustom ilt-repl-noisy t
  "If non-nil, ILT-REPL will beep on error."
  :type 'boolean)

(defcustom ilt-repl-prompt-read-only t
  "If non-nil, the ILT-REPL prompt is read only.
The read only region includes the newline before the prompt.
Setting this variable does not affect existing ILT-REPL runs.
This works by setting the buffer-local value of `comint-prompt-read-only'.
Setting that value directly affects new prompts in the current buffer.

If this option is enabled, then the safe way to temporarily
override the read-only-ness of ILT-REPL prompts is to call
`comint-kill-whole-line' or `comint-kill-region' with no
narrowing in effect.  This way you will be certain that none of
the remaining prompts will be accidentally messed up.  You may
wish to put something like the following in your init file:

\(add-hook \\='ilt-repl-mode-hook
          (lambda ()
             (define-key ilt-repl-map \"\\C-w\" \\='comint-kill-region)
             (define-key ilt-repl-map [C-S-backspace]
               \\='comint-kill-whole-line)))

If you set `comint-prompt-read-only' to t, you might wish to use
`comint-mode-hook' and `comint-mode-map' instead of
`ilt-repl-mode-hook' and `ilt-repl-map'.  That will affect all comint
buffers, including ILT-REPL buffers.  If you sometimes use ILT-REPL on
text-only terminals or with `emacs -nw', you might wish to use
another binding for `comint-kill-whole-line'."
  :type 'boolean
  :version "22.1")

(defcustom ilt-repl-prompt "ILT> "
  "Prompt used in ILT-REPL.
Setting this variable does not affect existing ILT-REPL runs.

Interrupting the ILT-REPL process with \\<ilt-repl-map>\\[comint-interrupt-subjob],
and then restarting it using \\[ilt-repl], makes the then current
default value affect _new_ prompts.  Unless the new prompt
differs only in text properties from the old one, ILT-REPL will no
longer recognize the old prompts.  However, executing \\[ilt-repl]
does not update the prompt of an *ilt-repl* buffer with a running process.
For ILT-REPL buffers that are not called `*ilt-repl*', you can execute
\\[inferior-emacs-lisp-mode] in that ILT-REPL buffer to update the value,
for new prompts.  This works even if the buffer has a running process."
  :type 'string)

(defvar ilt-repl-prompt-internal "ILT> "
  "Stored value of `ilt-repl-prompt' in the current ILT-REPL buffer.
This is an internal variable used by ILT-REPL.  Its purpose is to
prevent a running ILT-REPL process from being messed up when the user
customizes `ilt-repl-prompt'.")

(defcustom ilt-repl-dynamic-return t
  "Controls whether \\<ilt-repl-map>\\[ilt-repl-return] has intelligent behavior in ILT-REPL.
If non-nil, \\[ilt-repl-return] evaluates input for complete sexps, or inserts a newline
and indents for incomplete sexps.  If nil, always inserts newlines."
  :type 'boolean)

(defcustom ilt-repl-dynamic-multiline-inputs t
  "Force multiline inputs to start from column zero?
If non-nil, after entering the first line of an incomplete sexp, a newline
will be inserted after the prompt, moving the input to the next line.
This gives more frame width for large indented sexps, and allows functions
such as `edebug-defun' to work with such inputs."
  :type 'boolean)


(defcustom ilt-repl-mode-hook nil
  "Hooks to be run when ILT-REPL (`inferior-emacs-lisp-mode') is started."
  :options '(eldoc-mode)
  :type 'hook)

;; We define these symbols (that are only used buffer-locally in ilt-repl
;; buffers) this way to avoid having them be defined in the global
;; Emacs namespace.
(defvar *)
(put '* 'variable-documentation "Most recent value evaluated in ILT-REPL.")

(defvar **)
(put '** 'variable-documentation "Second-most-recent value evaluated in ILT-REPL.")

(defvar ***)
(put '*** 'variable-documentation "Third-most-recent value evaluated in ILT-REPL.")

(defvar ilt-repl-match-data nil
  "Match data saved at the end of last command.")

;; During ILT-REPL evaluation, *1 is the most recent value evaluated in
;; ILT-REPL.  Normally identical to `*'.  However, if the working buffer
;; is an ILT-REPL buffer, distinct from the process buffer, then `*' gives
;; the value in the working buffer, `*1' the value in the process
;; buffer.  The intended value is only accessible during ILT-REPL
;; evaluation.  *2 and *3 are the same for ** and ***.
(defvar *1)
(defvar *2)
(defvar *3)

;;; System variables

(defvar ilt-repl-working-buffer nil
  "Buffer in which ILT-REPL sexps will be evaluated.
This variable is buffer-local.")

(defvar ilt-repl-working-module nil
  "The current REPL language evaluation module.
This is backend dependent.")

(defvar ilt-repl-header
  (substitute-command-keys
   "*** Welcome to ILT-REPL ***  Type (describe-mode) or press \
\\[describe-mode] for help.\n")
  "Message to display when ILT-REPL is started.")

(defvar-keymap inferior-ilt-mode-map
  :doc "Keymap for ILT-REPL mode."
  "TAB"     #'ilt-repl-tab
  "RET"     #'ilt-repl-return
  "M-RET"   #'ilt-repl-return-for-effect
  "C-j"     #'ilt-repl-send-input
  "C-M-x"   #'ilt-compile-last-expr
  "M-TAB"   #'completion-at-point       ; lisp-interaction-mode
  ;; These bindings are from `lisp-mode-shared-map' -- can you inherit
  ;; from more than one keymap??
  "C-M-q"   #'indent-sexp
  "DEL"     #'backward-delete-char-untabify
  ;; Some convenience bindings for setting the working buffer
  "C-c C-b" #'ilt-repl-change-working-buffer
  "C-c C-f" #'ilt-repl-display-working-buffer
  "C-c C-v" #'ilt-repl-print-working-buffer
  "C-c C-i" #'ilt-repl-inspect-object-at-point)

(easy-menu-define inferior-ilt-mode-menu inferior-ilt-mode-map
  "ILT-REPL mode menu."
  '("ILT-REPL"
    ["Change Working Buffer" ilt-repl-change-working-buffer t]
    ["Display Working Buffer" ilt-repl-display-working-buffer t]
    ["Print Working Buffer" ilt-repl-print-working-buffer t]
    ["Set REPL module" ilt-repl-set-module t]))

(defvar ilt-repl-font-lock-keywords
  '(("\\(^\\*\\*\\*[^*]+\\*\\*\\*\\)\\(.*$\\)"
     (1 font-lock-comment-face)
     (2 font-lock-constant-face)))
  "Additional expressions to highlight in ILT-REPL buffers.")

;;; Completion stuff

(defun ilt-repl-tab ()
  "Indent or complete."
  (interactive)
  (if (or (eq (preceding-char) ?\n)
          (eq (char-syntax (preceding-char)) ?\s))
      (ilt-repl-indent-line)
    (completion-at-point)))

(defun ilt-repl-complete-filename ()
  "Dynamically complete filename before point, if in a string."
  ;;(when (nth 3 (parse-partial-sexp comint-last-input-start (point)))
  ;;  (comint-filename-completion))
  )

(defun ilt-repl-indent-line ()
  "Indent the current line as Lisp code if it is not a prompt line."
  (when (save-excursion (comint-bol t) (bolp))
    (lisp-indent-line)))

;;; Working buffer manipulation

(defun ilt-repl-print-working-buffer ()
  "Print the current ILT-REPL working buffer's name in the echo area."
  (interactive)
  (message "The current working buffer is: %s" (buffer-name ilt-repl-working-buffer)))

(defun ilt-repl-display-working-buffer ()
  "Display the current ILT-REPL working buffer.
Don't forget that selecting that buffer will change its value of `point'
to its value of `window-point'!"
  (interactive)
  (display-buffer ilt-repl-working-buffer)
  (ilt-repl-print-working-buffer))

(defun ilt-repl-change-working-buffer (buf)
  "Change the current ILT-REPL working buffer to BUF.
This is the buffer in which all sexps entered at the ILT-REPL prompt are
evaluated.  You can achieve the same effect with a call to
`set-buffer' at the ILT-REPL prompt."
  (interactive "bSet working buffer to: ")
  (let ((buffer (get-buffer buf)))
    (if (and buffer (buffer-live-p buffer))
        (setq ilt-repl-working-buffer buffer)
      (error "No such buffer: %S" buf)))
  (ilt-repl-print-working-buffer))

;;; Other bindings

(defun ilt-repl-return (&optional for-effect)
  "Newline and indent, or evaluate the sexp before the prompt.
Complete sexps are evaluated; for incomplete sexps inserts a newline
and indents.  If however `ilt-repl-dynamic-return' is nil, this always
simply inserts a newline."
  (interactive)
  (if ilt-repl-dynamic-return
      (ilt-repl-send-input for-effect)
      ;; (let ((state
      ;;        (save-excursion
      ;;          (end-of-line)
      ;;          (parse-partial-sexp (ilt-repl-pm)
      ;;                              (point)))))
      ;;   (if (and (< (car state) 1) (not (nth 3 state)))
      ;;       (ilt-repl-send-input for-effect)
      ;;     (when (and ilt-repl-dynamic-multiline-inputs
      ;;                (save-excursion
      ;;                  (beginning-of-line)
      ;;                  (looking-at-p comint-prompt-regexp)))
      ;;       (save-excursion
      ;;         (goto-char (ilt-repl-pm))
      ;;         (newline 1)))
      ;;     (newline-and-indent)))
    (newline)))

(defun ilt-repl-return-for-effect ()
  "Like `ilt-repl-return', but do not print the result."
  (interactive)
  (ilt-repl-return t))

(defvar ilt-repl-input)

(defun ilt-repl-input-sender (_proc input)
  ;; Just sets the variable ilt-repl-input, which is in the scope of
  ;; `ilt-repl-send-input's call.
  (setq ilt-repl-input input))

(defun ilt-repl-send-input (&optional for-effect)
  "Evaluate the Emacs Lisp expression after the prompt."
  (interactive)
  (let (ilt-repl-input)                     ; set by ilt-repl-input-sender
    (comint-send-input)                 ; update history, markers etc.
    (ilt-repl-eval-input ilt-repl-input for-effect)))

;;; Utility functions

(defun ilt-repl-is-whitespace-or-comment (string)
  "Return non-nil if STRING is all whitespace or a comment."
  (or (string= string "")
      (string-match-p "\\`[ \t\n]*\\(?:;.*\\)*\\'" string)))

;;; Evaluation

(defun ilt-repl-standard-output-impl (process)
  "Return a function to use for `standard-output' while in ilt-repl eval.
The returned function takes one character as input.  Passing nil
to this function instead of a character flushes the output
buffer.  Passing t appends a terminating newline if the buffer is
nonempty, then flushes the buffer."
  ;; Use an intermediate output buffer because doing redisplay for
  ;; each character we output is too expensive.  Set up a flush timer
  ;; so that users don't have to wait for whole lines to appear before
  ;; seeing output.
  (let* ((output-buffer nil)
         (flush-timer nil)
         (flush-buffer
          (lambda ()
            (comint-output-filter
             process
             (apply #'string (nreverse output-buffer)))
            (redisplay)
            (setf output-buffer nil)
            (when flush-timer
              (cancel-timer flush-timer)
              (setf flush-timer nil)))))
    (lambda (char)
      (let (flush-now)
        (cond ((and (eq char t) output-buffer)
               (push ?\n output-buffer)
               (setf flush-now t))
              ((characterp char)
               (push char output-buffer)))
        (if flush-now
            (funcall flush-buffer)
          (unless flush-timer
            (setf flush-timer (run-with-timer 0.1 nil flush-buffer))))))))

(defun ilt-repl-eval-input (input-string &optional for-effect)
  "Evaluate the expression INPUT-STRING, and pretty-print the result."
  ;; This is the function that actually `sends' the input to the
  ;; `inferior Lisp process'. All comint-send-input does is works out
  ;; what that input is.  What this function does is evaluates that
  ;; input and produces `output' which gets inserted into the buffer,
  ;; along with a new prompt.  A better way of doing this might have
  ;; been to actually send the output to the `cat' process, and write
  ;; this as in output filter that converted sexps in the output
  ;; stream to their evaluated value.  But that would have involved
  ;; more process coordination than I was happy to deal with.
  (let ((string input-string)          ; input expression, as a string
        pos                            ; End posn of parse in string
        result                         ; Result, or error message
        error-type                     ; string, nil if no error
        (output "")                    ; result to display
        (wbuf ilt-repl-working-buffer) ; current buffer after evaluation
        (pmark (ilt-repl-pm)))

    ;; Evaluate string
    (condition-case err
	(setq result (ilt--eval-sync string ilt-repl-working-module))
      ((error err)
       (setf error-type "Error")))

    (goto-char pmark)

    (unless error-type
      (condition-case err
          ;; Self-referential objects cause loops in the printer, so
          ;; trap quits here. May as well do errors, too

          (progn
	    (setq output
		  (propertize (assoc-default 'repr result)
			      'mouse-face 'highlight
			      'local-map (make-mode-line-mouse-map 'mouse-1 'ilt-repl-inspect-object-at-point)))
	    ;; Add the result object as a text property
	    ;; Then use from commands to inspect, etc.
	    (add-text-properties 0 (1- (length output)) `(ilt-object ,(ilt--parse-reference result))
				 output))
	
        (error
         (setq error-type "ILT-REPL Error")
         (setq result (format "Error during pretty-printing: %S" err)))
        (quit  (setq error-type "ILT-REPL Error")
               (setq result "Quit during pretty-printing"))))
    (if error-type
        (progn
          (when ilt-repl-noisy (ding))
          (setq output (concat output
                               "*** " error-type " ***  "
                               result)))
      ;; There was no error, so shift the *** values
      (setq *** (bound-and-true-p **))
      (setq ** (bound-and-true-p *))
      (setq * result))
    (when (or (not for-effect) (not (equal output "")))
      (setq output (concat output "\n")))
    (setq output (concat output ilt-repl-prompt-internal))
    (comint-output-filter (ilt-repl-process) output)))


;;; Process and marker utilities

(defun ilt-repl-process nil
  ;; Return the current buffer's process.
  (get-buffer-process (current-buffer)))

(defun ilt-repl-pm nil
  ;; Return the process mark of the current buffer.
  (process-mark (get-buffer-process (current-buffer))))

(defun ilt-repl-set-pm (pos)
  ;; Set the process mark in the current buffer to POS.
  (set-marker (process-mark (get-buffer-process (current-buffer))) pos))

;;; Major mode

(define-derived-mode inferior-ilt-mode comint-mode "ILT-REPL"
  "Major mode for interactively evaluating Emacs Lisp expressions.
Uses the interface provided by `comint-mode' (which see).

* \\<ilt-repl-map>\\[ilt-repl-send-input] evaluates the sexp following the prompt.  There must be at most
  one top level sexp per prompt.

* \\[ilt-repl-return] inserts a newline and indents, or evaluates a
  complete expression (but see variable `ilt-repl-dynamic-return').
  Inputs longer than one line are moved to the line following the
  prompt (but see variable `ilt-repl-dynamic-multiline-inputs').

* \\[ilt-repl-return-for-effect] works like `ilt-repl-return', except
  that it doesn't print the result of evaluating the input.  This
  functionality is useful when forms would generate voluminous
  output.

* \\[completion-at-point] completes Lisp symbols (or filenames, within strings),
  or indents the line if there is nothing to complete.

The current working buffer may be changed (with a call to `set-buffer',
or with \\[ilt-repl-change-working-buffer]), and its value is preserved between successive
evaluations.  In this way, expressions may be evaluated in a different
buffer than the *ilt-repl* buffer.  By default, its name is shown on the
mode line; you can always display it with \\[ilt-repl-print-working-buffer], or the buffer itself
with \\[ilt-repl-display-working-buffer].

During evaluations, the values of the variables `*', `**', and `***'
are the results of the previous, second previous and third previous
evaluations respectively.  If the working buffer is another ILT-REPL
buffer, then the values in the working buffer are used.  The variables
`*1', `*2' and `*3', yield the process buffer values.

If, at the start of evaluation, `standard-output' is t (the
default), `standard-output' is set to a special function that
causes output to be directed to the ilt-repl buffer.
`standard-output' is restored after evaluation unless explicitly
set to a different value during evaluation.  You can use (princ
VALUE) or (pp VALUE) to write to the ilt-repl buffer.

The behavior of ILT-REPL may be customized with the following variables:
* To stop beeping on error, set `ilt-repl-noisy' to nil.
* If you don't like the prompt, you can change it by setting `ilt-repl-prompt'.
* If you do not like that the prompt is (by default) read-only, set
  `ilt-repl-prompt-read-only' to nil.
* Set `ilt-repl-dynamic-return' to nil for bindings like `lisp-interaction-mode'.
* Entry to this mode runs `comint-mode-hook' and `ilt-repl-mode-hook'
 (in that order).

Customized bindings may be defined in `ilt-repl-map', which currently contains:
\\{ilt-repl-map}"
  :syntax-table emacs-lisp-mode-syntax-table

  (setq comint-prompt-regexp (concat "^" (regexp-quote ilt-repl-prompt)))
  (setq-local paragraph-separate "\\'")
  (setq-local paragraph-start comint-prompt-regexp)
  (setq comint-input-sender 'ilt-repl-input-sender)
  (setq comint-process-echoes nil)
  (dolist (f '(elisp-completion-at-point
               ilt-repl-complete-filename
               comint-replace-by-expanded-history))
    (add-hook 'completion-at-point-functions f nil t))
  (add-hook 'eldoc-documentation-functions
            #'elisp-eldoc-var-docstring nil t)
  (add-hook 'eldoc-documentation-functions
            #'elisp-eldoc-funcall nil t)
  (setq-local ilt-repl-prompt-internal ilt-repl-prompt)
  (setq-local comint-prompt-read-only ilt-repl-prompt-read-only)
  (setq comint-get-old-input 'ilt-repl-get-old-input)
  (setq-local comint-completion-addsuffix '("/" . ""))
  (setq mode-line-process '(":%s on " (:eval (buffer-name ilt-repl-working-buffer))))
  ;; Useful for `hs-minor-mode'.
  (setq-local comment-start ";")
  (setq-local comment-use-syntax t)
  (setq-local lexical-binding t)

  (setq-local indent-line-function #'ilt-repl-indent-line)
  (setq-local ilt-repl-working-buffer (current-buffer))
  (setq-local fill-paragraph-function #'lisp-fill-paragraph)

  ;; Value holders
  (setq-local * nil)
  (setq-local ** nil)
  (setq-local *** nil)
  (setq-local ilt-repl-match-data nil)
  (setq-local ilt-repl-working-module nil)

  ;; font-lock support
  (setq-local font-lock-defaults
              '(ilt-repl-font-lock-keywords nil nil ((?: . "w") (?- . "w") (?* . "w"))))

  ;; A dummy process to keep comint happy. It will never get any input
  (unless (comint-check-proc (current-buffer))
    ;; Was cat, but on non-Unix platforms that might not exist, so
    ;; use hexl instead, which is part of the Emacs distribution.
    (condition-case nil
        (start-process "ilt-repl" (current-buffer) "hexl")
      (file-error (start-process "ilt-repl" (current-buffer) "cat")))
    (set-process-query-on-exit-flag (ilt-repl-process) nil)
    (goto-char (point-max))

    ;; Lisp output can include raw characters that confuse comint's
    ;; carriage control code.
    (setq-local comint-inhibit-carriage-motion t)

    ;; Add a silly header
    (insert ilt-repl-header)
    (ilt-repl-set-pm (point-max))
    (unless comint-use-prompt-regexp
      (let ((inhibit-read-only t))
        (add-text-properties
         (point-min) (point-max)
         '(rear-nonsticky t field output inhibit-line-move-field-capture t))))
    (comint-output-filter (ilt-repl-process) ilt-repl-prompt-internal)
    (set-marker comint-last-input-start (ilt-repl-pm))
    (set-process-filter (get-buffer-process (current-buffer)) 'comint-output-filter)
    (add-hook 'inferior-ilt-mode #'ilt--connect-session)))

(defun ilt-repl-get-old-input ()
  "Return the previous input surrounding point."
  (save-excursion
    (beginning-of-line)
    (unless (looking-at-p comint-prompt-regexp)
      (re-search-backward comint-prompt-regexp))
    (comint-skip-prompt)
    (buffer-substring (point) (progn (forward-sexp 1) (point)))))

;;; User command

;;;###autoload
(defun ilt-repl (&optional buf-name)
  "Interactively evaluate Emacs Lisp expressions.
Switches to the buffer named BUF-NAME if provided (`*ilt-repl*' by default),
or creates it if it does not exist.
See `inferior-ilt-mode' for details."
  (interactive)
  (let (old-point
        (buf-name (or buf-name "*ilt-repl*")))
    (unless (comint-check-proc buf-name)
      (with-current-buffer (get-buffer-create buf-name)
        (unless (zerop (buffer-size)) (setq old-point (point)))
        (inferior-ilt-mode)
        (ilt-mode)))
    (pop-to-buffer-same-window buf-name)
    (when old-point (push-mark old-point))))

;;;###autoload
(defun ilt-repl-set-module (modulename)
  "Set the REPL module"
  (interactive (list (completing-read "Module: "
				      (mapcar (lambda (module)
						(assoc-default 'name module))
					      (ilt-list-modules-sync)))))
  (setq-local ilt-repl-working-module modulename)
  (setq-local ilt-repl-prompt-internal (format "%s> " modulename))
  (ilt-repl-send-input))

;;;###autoload
(defun ilt-repl-inspect-object-at-point ()
  (interactive)
  (when-let ((object (get-text-property (point) 'ilt-object)))
    (ilt-inspector-inspect object)))

(provide 'ilt-repl)

;;; ilt-repl.el ends here
