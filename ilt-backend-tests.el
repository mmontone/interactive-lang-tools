(require 'ilt)

;;; Code:

(defvar ilt-backend-tests (make-hash-table)
  "The list of tests for each backend.")

(defvar ilt-backend-tests-backend nil
  "The current backend being tested.
Has the form (ilt-backend-name . connection-args)")

(defvar ilt-backend-tests-connection nil
  "The backend connection for running tests.")

(defun ilt-backend-tests-for (backend)
  (or (gethash backend ilt-backend-tests)
      (error "No tests defined for backend: %s" backend)))

(ert-deftest ilt-backend-tests-connect-test ()
  "Test backend connection."
  (let ((connection
         (apply #'ilt--connect-backend
                (car ilt-backend-tests-backend)
                (cdr ilt-backend-tests-backend))))
    (should (typep connection 'ilt-connection))
    (setq ilt-backend-tests-connection connection)))

(ert-deftest ilt-backend-tests-evaluation-test ()
  "Perform some evaluations."
  (dolist (evaluation (assoc-default 'evaluations
                                     (ilt-backend-tests-for (car ilt-backend-tests-backend))))
    (let ((result (ilt--eval-sync (car evaluation) :connection ilt-backend-tests-connection))
          (test (cdr evaluation)))
      (should (cond
               ((functionp test)
                (funcall test result))
               ((stringp test)
                (string= (ilt-reference-repr (ilt--parse-reference result)) test))
               (t (error "Invalid test: %s" test)))))))

(defun ilt-backend-tests-test-backend (backend &rest args)
  "Run tests for BACKEND using ARGS."
  (let ((ilt-backend-tests-backend (cons backend args)))
    (ert "ilt-backend-tests-*")))

(provide 'ilt-backend-tests)
