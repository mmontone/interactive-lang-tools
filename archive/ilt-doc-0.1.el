;;; ilt-doc.el --- Documentation buffers for ILT definitions    -*- lexical-binding: t -*-

;; Copyright (C) 2021 Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; URL: https://github.com/mmontone/interactive-lang-tools
;; Keywords: help, lisp, ilt, common-lisp
;; Version: 0.1
;; Package-Requires: ((emacs "25") (ilt "0.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; ilt-doc is a ILT extension that improves ILT documentation buffers.

;;; Code:

(require 'ilt)
(require 'map)
(require 'button)
(require 'subr-x)

(defgroup ilt-doc nil
  "Common Lisp documentation browser."
  :prefix "ilt-doc-"
  :group 'ilt)

(defgroup ilt-doc-faces nil
  "Faces of ILT-DOC."
  :group 'ilt-doc)

(defface ilt-doc-heading-1
  '((t :weight bold
       :height 1.4
       ;;:underline t
       ))
  "Ilt help face for headings"
  :group 'ilt-doc-faces)

(defface ilt-doc-heading-2
  '((t :weight bold
       :height 1.3
       ;;:underline t
       ))
  "Ilt help face for headings"
  :group 'ilt-doc-faces)

(defface ilt-doc-heading-3
  '((t :weight bold
       :height 1.2
       ;;:underline t
       ))
  "Ilt help face for headings"
  :group 'ilt-doc-faces)

(defface ilt-doc-argument
  '((t (:inherit font-lock-type-face)))
  "Face for variables in Ilt help"
  :group 'ilt-doc-faces)

(defface ilt-doc-variable
  '((t :foreground "darkgreen"))
  "Face for variables in Ilt help"
  :group 'ilt-doc-faces)

(defface ilt-doc-keyword
  '((t :foreground "violet red"))
  "Face for keywords in Ilt help"
  :group 'ilt-doc-faces)

(defface ilt-doc-name
  '((t :foreground "orange"))
  "Face for name in Ilt help"
  :group 'ilt-doc-faces)

(defface ilt-doc-type
  '((t :foreground "purple"))
  "Face for type in Ilt help"
  :group 'ilt-doc-faces)

(defface ilt-doc-apropos-label
  '((t :weight bold))
  "Ilt help face for apropos items"
  :group 'ilt-doc-faces)

(defface ilt-doc-info
  '((t :foreground "green"))
  "Face for displaying ilt-doc information"
  :group 'ilt-doc-faces)

(defface ilt-doc-warning
  '((t :foreground "orange"))
  "Face for displaying ilt-doc warnings"
  :group 'ilt-doc-faces)

(defface ilt-doc-error
  '((t :foreground "red"))
  "Face for displaying ilt-doc errors"
  :group 'ilt-doc-faces)

(defface ilt-doc-button
  '((t (:box (:line-width 2 :color "dark grey") :background "light grey" :foreground "black")))
  "Face for ilt-doc buttons"
  :group 'ilt-doc-faces)

(defun ilt-doc--heading-1 (text)
  (propertize text 'face 'ilt-doc-heading-1))

(defun ilt-doc--heading-2 (text)
  (propertize text 'face 'ilt-doc-heading-2))

(defun ilt-doc--heading-3 (text)
  (propertize text 'face 'ilt-doc-heading-3))

(defun ilt-doc--horizontal-line (&rest width)
  (make-string (or width 80) ?\u2500))

(defun ilt-doc--info (text)
  (propertize text 'face 'ilt-doc-info))

(defun ilt-doc--warning (text)
  (propertize text 'face 'ilt-doc-warning))

(defun ilt-doc--error (text)
  (propertize text 'face 'ilt-doc-error))

(defun ilt-doc--propertize-docstring (string)
  (ilt-doc--propertize-links
   (ilt-doc--propertize-bare-links string)))

(defun ilt-doc--insert-documentation (info &optional package)
  (if ilt-doc-parse-docstrings
      (ilt-doc--format-parsed-docstring
       (cdr (assoc :parsed-documentation info))
       (or package (cdr (assoc :package info))))
    (insert (ilt-doc--propertize-docstring (cdr (assoc :documentation info))))))

;; copied from helpful.el library

(defun ilt-doc--button (text type &rest properties)
  ;; `make-text-button' mutates our string to add properties. Copy
  ;; TEXT to prevent mutating our arguments, and to support 'pure'
  ;; strings, which are read-only.
  (setq text (substring-no-properties text))
  (apply #'make-text-button
         text nil
         :type type
         properties))

(define-button-type 'ilt-doc-link-button
  'action #'ilt-doc--follow-link
  'follow-link t
  'help-echo "Follow this link")

(define-button-type 'ilt-doc-lookup-in-manuals-button
  'action #'ilt-doc--lookup-in-manuals
  'follow-link t
  'face 'ilt-doc-button
  'help-echo "Lookup in manuals")

(defun ilt-doc--lookup-in-manuals (btn)
  (funcall ilt-doc-lookup-in-manuals-function
           (prin1-to-string (button-get btn 'symbol))))

(defun ilt-doc--propertize-links (docstring)
  "Convert URL links in docstrings to buttons."
  (replace-regexp-in-string
   (rx "URL `" (group (*? any)) "'")
   (lambda (match)
     (let ((url (match-string 1 match)))
       (concat "URL "
               (ilt-doc--button
                url
                'ilt-doc-link-button
                'url url))))
   docstring))

(defun ilt-doc--propertize-bare-links (docstring)
  "Convert URL links in docstrings to buttons."
  (replace-regexp-in-string
   (rx (group (or string-start space "<"))
       (group "http" (? "s") "://" (+? (not (any space))))
       (group (? (any "." ">" ")"))
              (or space string-end ">")))
   (lambda (match)
     (let ((space-before (match-string 1 match))
           (url (match-string 2 match))
           (after (match-string 3 match)))
       (concat
        space-before
        (ilt-doc--button
         url
         'ilt-doc-link-button
         'url url)
        after)))
   docstring))

(defun ilt-doc--follow-link (button)
  "Follow the URL specified by BUTTON."
  (browse-url (button-get button 'url)))

;; helpful.el stuff ends here

(defun ilt-doc--qualify-cl-symbol-name (symbol-name &optional package)
  "Qualify a Common Lisp SYMBOL-NAME using PACKAGE.  If SYMBOL-NAME is already qualified, nothing is done.  If PACKAGE is not given, ILT-CURRENT-PACKAGE is used instead."
  (let ((package (or package (ilt-current-package))))
    (if (cl-position ?: symbol-name)
        ;; already qualified
        symbol-name
      (format "%s::%s" package symbol-name))))

(defun ilt-doc--format-parsed-docstring (docstring package)
  (dolist (word docstring)
    (cond
     ((stringp word) (insert (ilt-doc--propertize-docstring word)))
     ((and (listp word) (eql (first word) :arg))
      (insert (propertize (second word) 'face 'ilt-doc-argument)))
     ((and (listp word) (eql (first word) :fn))
      (insert-button (second word)
                     'action (lambda (btn)
                               (ilt-doc-function (third word)))
                     'follow-link t
                     'help-echo "Describe function"))
     ((and (listp word) (eql (first word) :macro))
      (insert-button (second word)
                     'action (lambda (btn)
                               (ilt-doc-macro (third word)))
                     'follow-link t
                     'help-echo "Describe function"))
     ((and (listp word) (eql (first word) :class))
      (insert-button (second word)
                     'action (lambda (btn)
                               (ilt-doc-class (third word)))
                     'follow-link t
                     'help-echo "Describe class"))
     ((and (listp word) (eql (first word) :key))
      (insert (propertize (second word) 'face 'ilt-doc-keyword)))
     ((and (listp word) (eql (first word) :var))
      (insert-button (second word)
                     'action (lambda (btn)
                               (ignore btn)
                               (ilt-doc-variable (third word)))
                     'follow-link t
                     'help-echo "Describe variable"))
     (t (error "Don't know how to format")))))

(defun ilt-doc-symbol (symbol-name)
  "Open a help buffer for each kind of SYMBOL-NAME."
  (interactive (list (ilt-read-symbol-name "Describe symbol: ")))
  (when (not symbol-name)
    (error "No symbol given"))
  (let ((symbol-infos (ilt-eval `(swank-help:read-emacs-symbol-info (cl:read-from-string ,(ilt-qualify-cl-symbol-name symbol-name))))))
    (dolist (symbol-info symbol-infos)
      (cl-case (cdr (assoc :type symbol-info))
        (:function (ilt-doc-function symbol-name))
        (:generic-function (ilt-doc-generic-function symbol-name))
        (:macro (ilt-doc-macro symbol-name))
        (:package (ilt-doc-package symbol-name))
        (:variable (ilt-doc-variable symbol-name))
        (:class (ilt-doc-class symbol-name))
        (t (error "TODO"))))))

;;(ilt-doc-symbol "ALEXANDRIA:FLATTEN")
;;(ilt-doc-symbol "ALEXANDRIA:WITH-GENSYMS")

(defun ilt-doc--first-line (string)
  "Return the first line of the `STRING'."
  (let ((pos (cl-position ?\n string)))
    (if (null pos) string (cl-subseq string 0 pos))))

(defun ilt-doc--kill-current-buffer ()
  (interactive)
  (kill-buffer (current-buffer)))

(defun ilt-doc-quit ()
  "Kill all ilt-doc buffers at once."
  (interactive)
  (mapcar 'kill-buffer
          (cl-remove-if-not
           (lambda (buffer)
             (string-prefix-p "*ilt-doc" (buffer-name buffer)))
           (buffer-list))))

(defun ilt-doc--open-buffer ()
  (let ((buffer (current-buffer)))
    (setq buffer-read-only t)
    (local-set-key "q" 'ilt-doc--kill-current-buffer)
    (buffer-disable-undo)
    (set (make-local-variable 'kill-buffer-query-functions) nil)
    (ilt-mode)
    (ilt-doc-mode)
    (goto-char 0)
    (pop-to-buffer buffer)))

(cl-defun ilt-doc-package (package-name)
  "Display information about Common Lisp package named PACKAGE-NAME."
  (interactive (list (ilt-read-package-name "Describe package: ")))
  (when (not package-name)
    (error "No package name given"))

  (let ((buffer-name (format "*ilt-doc: %s package*" package-name)))
    (when (get-buffer buffer-name)
      (pop-to-buffer buffer-name)
      (cl-return-from ilt-doc-package))
    (let* ((package-info (ilt-eval `(swank-help:read-emacs-package-info ,package-name)))
           (buffer (get-buffer-create buffer-name)))
      (with-current-buffer buffer
        (insert (ilt-doc--heading-1 (upcase (string-trim package-name))))
        (newline 2)
        (insert (format "This is a Common Lisp package with %d external symbols" (length (cdr (assoc :external-symbols package-info)))))
        (newline 2)
        (when (cdr (assoc :documentation package-info))
          (insert (ilt-doc--propertize-docstring (cdr (assoc :documentation package-info))))
          (newline 2))

        (cl-flet ((goto-source (btn)
                    (ilt-edit-definition-other-window package-name)))
          (insert-button "Source"
                         'action (function goto-source)
                         'face 'ilt-doc-button
                         'follow-link t
                         'help-echo "Go to package source code"))
        (newline 2)

        (insert (ilt-doc--heading-2 "Exported definitions"))
        (newline 2)
        (cl-flet ((format-exported-definition
                    (symbol-info)
                    (insert (propertize (cl-subseq (symbol-name (cdr (assoc :type symbol-info))) 1) 'face 'ilt-doc-type))
                    (insert " ")
                    (insert-button (format "%s" (cdr (assoc :name symbol-info)))
                                   'action (lambda (btn)
                                             (ignore btn)
                                             (ilt-doc-symbol (prin1-to-string (cdr (assoc :symbol symbol-info)))))
                                   'follow-link t
                                   'help-echo "Describe symbol")
                    (newline)
                    (if (cdr (assoc :documentation symbol-info))
                        (insert (ilt-doc--first-line (cdr (assoc :documentation symbol-info))))
                      (insert "Not documented"))
                    (newline)
                    (insert (ilt-doc--horizontal-line))
                    (newline)))
          (let ((def-types '(("Variables" . :variable)
                             ("Classes" . :class)
                             ("Macros" . :macro)
                             ("Functions" . :function)
                             ("Generic functions" . :generic-function))))
            (dolist (def-type def-types)
              (let ((symbol-infos (cl-remove-if-not (lambda (x)
                                                      (cl-equalp (cdr (assoc :type x)) (cdr def-type)))
                                                    (cdr (assoc :external-symbols package-info)))))
                (when symbol-infos
                  (insert (ilt-doc--heading-3 (car def-type)))
                  (newline 2)
                  (dolist (symbol-info symbol-infos)
                    (format-exported-definition symbol-info))
                  (newline 2))))))
        (ilt-doc--open-buffer)
        nil))))

;;(ilt-doc-package "ALEXANDRIA")

(cl-defun ilt-doc-packages ()
  "Display information about Common Lisp packages."
  (interactive)

  (let ((buffer-name "*ilt-doc: COMMON LISP packages*"))
    (when (get-buffer buffer-name)
      (pop-to-buffer buffer-name)
      (cl-return-from ilt-doc-packages))

    (let* ((packages-info (ilt-eval `(swank-help:read-emacs-packages-info)))
           (buffer (get-buffer-create buffer-name)))
      (with-current-buffer buffer
        (dolist (package-info packages-info)
          (let ((package-name (cdr (assoc :name package-info))))
            (insert-button package-name
                           'action (lambda (btn)
                                     (ignore btn)
                                     (ilt-doc-package package-name))
                           'follow-link t
                           'help-echo "Describe package"))
          (newline)
          (when (cdr (assoc :documentation package-info))
            (insert (cdr (assoc :documentation package-info)))
            (newline)))
        (ilt-doc--open-buffer)
        nil))))

(cl-defun ilt-doc-systems ()
  "Display information about registered ASDF systems."

  (interactive)

  (let ((buffer-name "*ilt-doc: registered ASDF systems*"))
    (when (get-buffer buffer-name)
      (pop-to-buffer buffer-name)
      (cl-return-from ilt-doc-systems))

    (let* ((systems-info (ilt-eval `(swank-help:read-emacs-systems-info)))
           (buffer (get-buffer-create buffer-name)))
      (with-current-buffer buffer
        (dolist (system-info systems-info)
          (lexical-let ((system-name (cdr (assoc :name system-info))))
            (insert-button system-name
                           'action (lambda (btn)
                                     (ignore btn)
                                     (ilt-doc-system system-name))
                           'follow-link t
                           'help-echo "Describe system"))
          (newline)
          (when (cdr (assoc :documentation system-info))
            (insert (cdr (assoc :documentation system-info)))
            (newline)))
        (ilt-doc--open-buffer)
        nil))))

(defun ilt-doc-function (symbol-name)
  "Display documentation about Common Lisp function bound to SYMBOL-NAME."
  (interactive (list (ilt-read-symbol-name "Describe function: ")))
  (when (not symbol-name)
    (error "No symbol given"))
  (let ((symbol-info (ilt-eval `(swank-help:read-emacs-symbol-info (cl:read-from-string ,(ilt-qualify-cl-symbol-name symbol-name)) :function))))
    (when (null symbol-info)
      (error "Could not read symbol informartion: %s" symbol-name))
    (ilt-doc--funcallable symbol-name symbol-info :function)))

(defun ilt-doc-macro (symbol-name)
  "Display documentation about Common Lisp macro bound to SYMBOL-NAME."
  (interactive (list (ilt-read-symbol-name "Describe macro: ")))
  (when (not symbol-name)
    (error "No symbol given"))
  (let ((symbol-info (ilt-eval `(swank-help:read-emacs-symbol-info (cl:read-from-string ,(ilt-qualify-cl-symbol-name symbol-name)) :macro))))
    (when (null symbol-info)
      (error "Could not read symbol information: %s" symbol-name))
    (ilt-doc--funcallable symbol-name symbol-info
                           :macro
                           'extra-buttons
                           (lambda ()
                             (cl-flet ((browse-expanders (btn)
                                         (ignore btn)
                                         (ilt-who-macroexpands (prin1-to-string (cdr (assoc :symbol symbol-info))))))
                               (insert-button "Expanders"
                                              'face 'ilt-doc-button
                                              'action (function browse-expanders)
                                              'follow-link t
                                              'help-echo "Show all known expanders of the macro"))
                             (insert " ")))))

;; (ilt-doc-macro "ALEXANDRIA:WITH-GENSYMS")

(defun ilt-doc-generic-function (symbol-name)
  "Display documentation about Common Lisp generic function bound to SYMBOL-NAME."
  (interactive (list (ilt-read-symbol-name "Describe generic function: ")))
  (when (not symbol-name)
    (error "No symbol given"))
  (let ((symbol-info (ilt-eval `(swank-help:read-emacs-symbol-info (cl:read-from-string ,(ilt-qualify-cl-symbol-name symbol-name)) :generic-function))))
    (when (null symbol-info)
      (error "Could not read symbol informartion: %s" symbol-name))
    (ilt-doc--funcallable
     symbol-name symbol-info
     :generic-function
     'continuation
     (lambda ()
       (let ((methods (ilt-find-definitions (ilt-qualify-cl-symbol-name symbol-name))))
         (newline 2)
         (insert (ilt-doc--heading-2 "Methods"))
         (newline 2)
         (cl-loop for (label location) in methods do
                  (ilt-insert-propertized
                   (list 'ilt-location location
                         'face 'font-lock-keyword-face)
                   "  " (ilt-one-line-ify label) "\n")))
       (ilt-xref-mode)))))

;; (ilt-doc-generic-function "CL:PRINT-OBJECT")

;; TODO: remove the following function. I think it is better to implement individual functions for each type of funcallable (macro, function, generic-functions), as they have significant differences.
(cl-defun ilt-doc--funcallable (symbol-name symbol-info function-type &rest args)
  "Display documentation about Common Lisp the funcallable FUNCTION-TYPE to SYMBOL-NAME. SYMBOL-INFO contains the collected SWANK documentation. ARGS contains additional arguments, like 'extra-buttons."
  (let* ((function-type-name (cl-subseq (symbol-name function-type) 1))
         (buffer-name (format "*ilt-doc: %s %s*" symbol-name function-type-name)))
    (when (get-buffer buffer-name)
      (pop-to-buffer buffer-name)
      (cl-return-from ilt-doc--funcallable))

    (let* ((package-name (cdr (assoc :package symbol-info)))
           (buffer (get-buffer-create buffer-name)))
      (with-current-buffer buffer
        (insert (ilt-doc--heading-1 (cdr (assoc :name symbol-info))))
        (newline 2)
        (insert (format "This is a %s in package " function-type-name))
        (insert-button package-name
                       'action (lambda (btn)
                                 (ignore btn)
                                 (ilt-doc-package package-name))
                       'follow-link t
                       'help-echo "Describe package")
        (newline 2)
        (insert (ilt-doc--heading-3 "Signature"))
        (newline)
        (insert (ilt-doc--highlight-syntax (cdr (assoc :args symbol-info))))
        (newline 2)

        (when (cdr (assoc :documentation symbol-info))
          (ilt-doc--insert-documentation symbol-info)
          (newline 2))

        (cl-flet ((goto-source (btn)
                    (ignore btn)
                    (ilt-edit-definition-other-window (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "Source"
                         'action (function goto-source)
                         'face 'ilt-doc-button
                         'follow-link t
                         'help-echo "Go to definition source code"))
        (insert " ")

        (cl-flet ((browse-references (btn)
                    (ilt-who-calls (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "References"
                         'action (function browse-references)
                         'face 'ilt-doc-button
                         'follow-link t
                         'help-echo "Browse references"))
        (insert " ")

        (cl-flet ((disassemble-function (btn)
                    (ilt-disassemble-symbol (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "Disassemble"
                         'action (function disassemble-function)
                         'face 'ilt-doc-button
                         'follow-link t
                         'help-echo "Disassemble function"))
        (insert " ")

        (when (cl-getf args 'extra-buttons)
          (funcall (cl-getf args 'extra-buttons)))

        (insert (ilt-doc--button "Lookup in manuals"
                                  'ilt-doc-lookup-in-manuals-button
                                  'symbol (cdr (assoc :symbol symbol-info))))
        (insert " ")

        (when (cl-member (cdr (assoc :package symbol-info))
                         '("COMMON-LISP" "CL") :test 'equalp)
          (cl-flet ((lookup-in-hyperspec (btn)
                      (ignore btn)
                      (ilt-hyperspec-lookup
                       (prin1-to-string (cdr (assoc :symbol symbol-info))))))
            (insert-button "Lookup in Hyperspec"
                           'face 'ilt-doc-button
                           'action (function lookup-in-hyperspec)
                           'follow-link t
                           'help-echo "Lookup variable in Hyperspec")))

        ;; TODO: add a collapsible extra section with debugging actions, like toggle tracing, toggle profiling, perhaps disassemble too.

        (when (cl-getf args 'continuation)
          (funcall (cl-getf args 'continuation)))

        (ilt-doc--open-buffer)
        nil))))

;;(ilt-doc-function "CL:REMOVE")
;;(ilt-doc-function "ALEXANDRIA:FLATTEN")
;;(ilt-doc-function "SPLIT-SEQUENCE:SPLIT-SEQUENCE")
;;(ilt-doc-macro "ALEXANDRIA:WITH-GENSYMS")

(cl-defun ilt-doc-variable (symbol-name)
  "Display documentation about Common Lisp variable bound to SYMBOL-NAME."
  (interactive (list (ilt-read-symbol-name "Describe variable: ")))
  (when (not symbol-name)
    (error "No symbol given"))

  (let ((buffer-name (format "*ilt-doc: %s variable*" symbol-name)))
    (when (get-buffer buffer-name)
      (pop-to-buffer buffer-name)
      (cl-return-from ilt-doc-variable))

    (let* ((symbol-info (ilt-eval `(swank-help:read-emacs-symbol-info (cl:read-from-string ,(ilt-qualify-cl-symbol-name symbol-name)) :variable)))
           (package-name (cdr (assoc :package symbol-info)))
           (buffer (get-buffer-create buffer-name)))
      (when (null symbol-info)
        (error "Could not read variable info"))
      (with-current-buffer buffer
        (insert (ilt-doc--heading-1 (cdr (assoc :name symbol-info))))
        (newline 2)
        (insert (format "This is a VARIABLE in package "))
        (insert-button package-name
                       'action (lambda (btn)
                                 (ignore btn)
                                 (ilt-doc-package package-name))
                       'follow-link t
                       'help-echo "Describe package")
        (newline 2)
        (when (cdr (assoc :documentation symbol-info))
          (ilt-doc--insert-documentation symbol-info)
          (newline 2))

        (if (not (cdr (assoc :boundp symbol-info)))
            (insert (ilt-doc--warning "The variable is UNBOUND"))
          (progn
            (insert (propertize "Value: " 'face 'bold))
            (insert (ilt-doc--info (cdr (assoc :value symbol-info))))))
        (newline 2)

        (cl-flet ((goto-source (btn)
                    (ignore btn)
                    (ilt-edit-definition-other-window (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "Source"
                         'action (function goto-source)
                         'face 'ilt-doc-button
                         'follow-link t
                         'help-echo "Go to definition source code"))
        (insert " ")

        (cl-flet ((browse-references (btn)
                    (ilt-who-references (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "References"
                         'face 'ilt-doc-button
                         'action (function browse-references)
                         'follow-link t
                         'help-echo "Browse references"))
        (insert " ")

        (cl-flet ((browse-binders (btn)
                    (ilt-who-binds (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "Binders"
                         'face 'ilt-doc-button
                         'action (function browse-binders)
                         'follow-link t
                         'help-echo "Show all known binders of the global variable"))
        (insert " ")

        (cl-flet ((browse-setters (btn)
                    (ilt-who-sets (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "Setters"
                         'face 'ilt-doc-button
                         'action (function browse-setters)
                         'follow-link t
                         'help-echo "Show all known setters of the global variable"))
        (insert " ")

        (insert (ilt-doc--button "Lookup in manuals"
                                  'ilt-doc-lookup-in-manuals-button
                                  'symbol (cdr (assoc :symbol symbol-info))))

        (insert " ")

        (when (cl-member (cdr (assoc :package symbol-info))
                         '("COMMON-LISP" "CL") :test 'equalp)
          (cl-flet ((lookup-in-hyperspec (btn)
                      (ignore btn)
                      (ilt-hyperspec-lookup
                       (prin1-to-string (cdr (assoc :symbol symbol-info))))))
            (insert-button "Lookup in Hyperspec"
                           'face 'ilt-doc-button
                           'action (function lookup-in-hyperspec)
                           'follow-link t
                           'help-echo "Lookup variable in Hyperspec")))

        (ilt-doc--open-buffer)
        nil))))

;; (ilt-doc-variable "*STANDARD-OUTPUT*")

(cl-defun ilt-doc-class (symbol-name)
  "Display documentation about Common Lisp class bound to SYMBOL-NAME."
  (interactive (list (ilt-read-symbol-name "Describe class: ")))
  (when (not symbol-name)
    (error "No symbol given"))

  (let ((buffer-name (format "*ilt-doc: %s class*" symbol-name)))
    (when (get-buffer buffer-name)
      (pop-to-buffer buffer-name)
      (cl-return-from ilt-doc-class))

    (let* ((symbol-info (ilt-eval `(swank-help:read-emacs-symbol-info (cl:read-from-string ,(ilt-qualify-cl-symbol-name symbol-name)) :class)))
           (package-name (cdr (assoc :package symbol-info)))
           (buffer (get-buffer-create buffer-name)))
      (when (null symbol-info)
        (error "Could not read class info"))
      (with-current-buffer buffer
        (insert (ilt-doc--heading-1 (cdr (assoc :name symbol-info))))
        (newline 2)
        (insert (format "This is a CLASS in package "))
        (insert-button package-name
                       'action (lambda (btn)
                                 (ilt-doc-package package-name))
                       'follow-link t
                       'help-echo "Describe package")
        (newline 2)

        (when (cdr (assoc :documentation symbol-info))
          (ilt-doc--insert-documentation symbol-info)
          (newline 2))

        ;; buttons
        (cl-flet ((goto-source (btn)
                    (ilt-edit-definition-other-window (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "Source"
                         'face 'ilt-doc-button
                         'action (function goto-source)
                         'follow-link t
                         'help-echo "Go to definition source code"))
        (insert " ")
        (cl-flet ((browse-references (btn)
                    (ilt-who-references (prin1-to-string (cdr (assoc :symbol symbol-info))))))
          (insert-button "References"
                         'face 'ilt-doc-button
                         'action (function browse-references)
                         'follow-link t
                         'help-echo "Browse references"))
        (insert " ")

        (insert (ilt-doc--button "Lookup in manuals"
                                  'ilt-doc-lookup-in-manuals-button
                                  'symbol (cdr (assoc :symbol symbol-info))))
        (insert " ")

        (when (cl-member (cdr (assoc :package symbol-info))
                         '("COMMON-LISP" "CL") :test 'equalp)
          (cl-flet ((lookup-in-hyperspec (btn)
                      (ilt-hyperspec-lookup
                       (prin1-to-string (cdr (assoc :symbol symbol-info))))))
            (insert-button "Lookup in Hyperspec"
                           'face 'ilt-doc-button
                           'action (function lookup-in-hyperspec)
                           'follow-link t
                           'help-echo "Lookup variable in Hyperspec")))

        (newline 2)

        (insert (ilt-doc--heading-2 "Direct superclasses"))
        (newline 2)
        (dolist (class-name (cdr (assoc :direct-superclasses symbol-info)))
          (insert-button (upcase (symbol-name class-name))
                         'action (lambda (btn)
                                   (ignore btn)
                                   (ilt-doc-class (symbol-name class-name)))
                         'follow-link t
                         'help-echo "Describe class")
          (insert " "))
        (newline 2)

        ;; TODO: show a collapsable (outline-mode?) section with more information about the class
        ;; like class descendants and list of subclasses
        ;; let's show direct-subclasses for now, with a limit
        (let ((max-subclasses 25)
              (subclasses (cdr (assoc :direct-subclasses symbol-info))))
          (insert (ilt-doc--heading-2 "Direct subclasses"))
          (newline 2)
          (dolist (class-name (subseq subclasses 0 (min max-subclasses (length subclasses))))
            (insert-button (upcase (symbol-name class-name))
                           'action (lambda (btn)
                                     (ignore btn)
                                     (ilt-doc-class (symbol-name class-name)))
                           'follow-link t
                           'help-echo "Describe class")
            (insert " "))
          (when (> (length subclasses) max-subclasses)
            (insert "and more"))
          (newline 2))

        ;; TODO: show more information about slots
        (let ((slots (cdr (assoc :slots symbol-info))))
          (insert (ilt-doc--heading-2 "Slots"))
          (newline 2)
          (if (zerop (length slots))
              (progn (insert "No slots") (newline))
            (dolist (slot slots)
              (insert (propertize (format "- %s" (cdr (assoc :name slot))) 'face 'bold))
              (newline)
              (when (cdr (assoc :documentation slot))
                (ilt-doc--insert-documentation slot (cdr (assoc :package symbol-info)))
                (newline))))
          (newline))

        (let ((methods-start (point))
              (methods (cdr (assoc :methods symbol-info))))
          (insert (ilt-doc--heading-2 "Methods"))
          (make-text-button methods-start (point)
                            'action (lambda (btn)
                                      (ignore btn)
                                      (goto-char methods-start)
                                      (outline-toggle-children))
                            'follow-link t)
          (newline 2)
          (if (zerop (length methods))
              (insert "No methods")
            (dolist (symbol-info methods)
              (insert-button (format "%s" (upcase (prin1-to-string (cdr (assoc :name symbol-info)))))
                             'action (lambda (btn)
                                       (ignore btn)
                                       (ilt-doc-symbol (prin1-to-string (cdr (assoc :name symbol-info)))))
                             'follow-link t
                             'help-echo "Describe symbol")
              (newline)
              (if (cdr (assoc :documentation symbol-info))
                  (insert (ilt-doc--first-line (cdr (assoc :documentation symbol-info))))
                (insert "Not documented"))
              (newline)
              (insert (ilt-doc--horizontal-line))
              (newline))))

        ;; Outlines configuration
        ;;(setq outline-regexp "Methods")
        ;;(outline-minor-mode)
        ;;(outline-hide-body)

        (ilt-doc--open-buffer)

        nil))))

;;(ilt-doc-class "HUNCHENTOOT:ACCEPTOR")

;; This was copied from help.el
(defun ilt-doc--highlight-syntax (source &optional mode)
  "Return a propertized version of SOURCE in MODE."
  (unless mode
    (setq mode #'lisp-mode))
  (if (or
       (< (length source) 5000)
       (eq mode 'emacs-lisp-mode))
      (with-temp-buffer
        (insert source)

        ;; Switch to major-mode MODE, but don't run any hooks.
        (delay-mode-hooks (funcall mode))

        ;; `delayed-mode-hooks' contains mode hooks like
        ;; `emacs-lisp-mode-hook'. Build a list of functions that are run
        ;; when the mode hooks run.
        (let (hook-funcs)
          (dolist (hook delayed-mode-hooks)
            (let ((funcs (symbol-value hook)))
              (setq hook-funcs (append hook-funcs funcs)))))

        (if (fboundp 'font-lock-ensure)
            (font-lock-ensure)
          (with-no-warnings
            (font-lock-fontify-buffer)))
        (buffer-string))
    ;; SOURCE was too long to highlight in a reasonable amount of
    ;; time.
    source))

(cl-defun ilt-doc-system (system-name)
  "Display documentation about ASDF system named SYSTEM-NAME."
  (interactive (list (ilt-read-system-name "Describe system")))
  (when (not system-name)
    (error "No system name given"))

  (let ((buffer-name (format "*ilt-doc: %s system*" system-name)))
    (when (get-buffer buffer-name)
      (pop-to-buffer buffer-name)
      (cl-return-from ilt-doc-system))

    (let* ((system-info (ilt-eval `(swank-help:read-emacs-system-info ,system-name)))
           (buffer (get-buffer-create buffer-name)))
      (with-current-buffer buffer
        (insert (ilt-doc--heading-1 (upcase system-name)))
        (newline 2)
        (insert (format "This is a Common Lisp ASDF system with %d dependencies" (length (cdr (assoc :dependencies system-info)))))
        (newline)
        (if (cdr (assoc :loaded-p system-info))
            (insert (ilt-doc--info "This system is already loaded."))
          (insert (ilt-doc--error "This system is not loaded.")))
        (newline 2)
        (when (cdr (assoc :documentation system-info))
          (insert (ilt-doc--propertize-docstring (cdr (assoc :documentation system-info))))
          (newline 2))

        ;; buttons

        (cl-flet ((browse-system (btn)
                    (ilt-browse-system system-name)))
          (insert-button "Browse"
                         'action (function browse-system)
                         'follow-link t
                         'help-echo "Browse system"))

        (insert " ")

        (when (not (cdr (assoc :loaded-p system-info)))
          (cl-flet ((load-system (btn)
                      (ignore btn)
                      (ilt-load-system system-name)))
            (insert-button "Load"
                           'action (function load-system)
                           'follow-link t
                           'help-echo "Load system")))

        (newline 2)

        (insert (ilt-doc--heading-2 "Dependencies"))
        (newline 2)
        (if (zerop (length (cdr (assoc :dependencies system-info))))
            (insert "It has no dependencies")
          ;; else
          (dolist (dependency (cdr (assoc :dependencies system-info)))
            (insert "* ")
            (insert-button dependency
                           'action (lambda (btn)
                                     (ignore btn)
                                     (ilt-doc-system dependency))
                           'follow-link t
                           'help-echo "Describe system")
            (newline)))

        (when (and (cdr (assoc :loaded-p system-info))
                   (cdr (assoc :packages system-info)))
          (newline 2)
          (insert (ilt-doc--heading-2 "Packages"))
          (newline 2)
          (dolist (package-name (cdr (assoc :packages system-info)))
            (insert-button package-name
                           'action (lambda (btn)
                                     (ignore btn)
                                     (ilt-doc-package package-name))
                           'follow-link t
                           'help-echo "Describe package")
            (insert " ")))

        (ilt-doc--open-buffer)
        nil))))

;;(ilt-doc-system "alexandria")

(defun ilt-doc-print-apropos (plists)
  (dolist (plist plists)
    (let ((designator (plist-get plist :designator)))
      (cl-assert designator)
      ;;(ilt-insert-propertized `(face ilt-apropos-symbol) designator)
      (insert (propertize designator 'face 'ilt-apropos-symbol)))
    (terpri)
    (cl-loop for (prop value) on plist by #'cddr
             unless (eq prop :designator) do
             (let ((namespace (cadr (or (assq prop ilt-apropos-namespaces)
                                        (error "Unknown property: %S" prop))))
                   (start (point)))
               (princ "  ")
               (ilt-insert-propertized `(face ilt-doc-apropos-label) namespace)
               (princ ": ")
               (princ (cl-etypecase value
                        (string value)
                        ((member nil :not-documented) "(not documented)")))
               (let ((designator (plist-get plist :designator)))
                 (add-text-properties
                  start (point)
                  (list 'type prop
                        'action (lambda (btn) (ignore btn) (ilt-doc-symbol designator))
                        'weight 'bold
                        'button t
                        'apropos-label namespace
                        'item designator)))
               (terpri)))))

(defun ilt-doc-show-apropos (plists string package summary)
  (if (null plists)
      (message "No apropos matches for %S" string)
    (ilt-with-popup-buffer ((ilt-buffer-name :apropos)
                            :package package :connection t
                            :mode 'apropos-mode)
                           (if (boundp 'header-line-format)
                               (setq header-line-format summary)
                             (insert summary "\n\n"))
                           (ilt-set-truncate-lines)
                           (ilt-doc-print-apropos plists)
                           (set-syntax-table lisp-mode-syntax-table)
                           (goto-char (point-min)))))

(defun ilt-doc-apropos (string &optional only-external-p package
                                case-sensitive-p)
  "Show all bound symbols whose names match STRING. With prefix
arg, you're interactively asked for parameters of the search."
  (interactive
   (if current-prefix-arg
       (list (read-string "ILT Apropos: ")
             (y-or-n-p "External symbols only? ")
             (let ((pkg (ilt-read-package-name "Package: ")))
               (if (string= pkg "") nil pkg))
             (y-or-n-p "Case-sensitive? "))
     (list (read-string "ILT Apropos: ") t nil nil)))
  (let ((buffer-package (or package (ilt-current-package))))
    (ilt-eval-async
     `(swank:apropos-list-for-emacs ,string ,only-external-p
                                    ,case-sensitive-p ',package)
     (ilt-rcurry #'ilt-doc-show-apropos string buffer-package
                 (ilt-apropos-summary string case-sensitive-p
                                      package only-external-p)))))

(defun ilt-doc-apropos-all ()
  "Shortcut for (ilt-doc-apropos <string> nil nil)"
  (interactive)
  (ilt-doc-apropos (read-string "ILT Apropos: ") nil nil))

(defun ilt-doc-apropos-package (package &optional internal)
  "Show apropos listing for symbols in PACKAGE.
With prefix argument include internal symbols."
  (interactive (list (let ((pkg (ilt-read-package-name "Package: ")))
                       (if (string= pkg "") (ilt-current-package) pkg))
                     current-prefix-arg))
  (ilt-doc-apropos "" (not internal) package))

(defun ilt-doc-apropos-documentation (pattern &optional package)
  "Show symbols whose documentation contains matches for PATTERN.
PATTERN can be a word, a list of words (separated by spaces),
or a regexp (using some regexp special characters).  If it is a word,
search for matches for that word as a substring.  If it is a list of words,
search for matches for any two (or more) of those words."
  (interactive (list (apropos-read-pattern "documentation")))

  (let ((buffer-package (or package (ilt-current-package))))
    (ilt-eval-async
     `(swank-help:apropos-documentation-for-emacs
       ',pattern t
       nil nil)
     (ilt-rcurry #'ilt-doc-show-apropos (first pattern) buffer-package
                 (ilt-apropos-summary pattern nil
                                      nil t)))))

(defun ilt-doc ()
  (interactive)
  (ilt-doc-systems))

(defvar ilt-doc-mode-map
  (let ((map (make-keymap)))
    (define-key map "q" 'ilt-doc--kill-current-buffer)
    (define-key map "Q" 'ilt-doc-quit)
    map))

(define-minor-mode ilt-doc-mode
  "Quicklisp systems minor mode."
  :init-value nil
  :lighter " Ilt-Doc"
  :keymap ilt-doc-mode-map
  :group 'ilt-doc)

(easy-menu-define
  ilt-doc-mode-menu ilt-doc-mode-map
  "Menu for Ilt-Doc"
  '("ILT Help"
    ["Browse systems" ilt-doc-systems
     :help "Browse registered ASDF systems"]
    ["Browse packages" ilt-doc-packages
     :help "Browse the list of loaded packages"]
    "---"
    ["Describe symbol..." ilt-doc-symbol
     :help "Show documentation of symbol"]
    ["Describe function..." ilt-doc-function
     :help "Show documentation of function"]
    ["Describe package..." ilt-doc-package
     :help "Show package documentation"]
    ["Describe system..." ilt-doc-system
     :help "Show ASDF system documentation"]
    "---"
    [ "Lookup Documentation..." ilt-documentation-lookup t ]
    [ "Apropos..."              ilt-doc-apropos t]
    [ "Apropos all..."          ilt-doc-apropos-all t]
    [ "Apropos Package..."      ilt-doc-apropos-package t]
    [ "Apropos documentation..." ilt-doc-apropos-documentation
      :help "Search in docstrings"]
    "---"
    ["Quit" ilt-doc-quit
     :help "Quit ILT help"]))

(easy-menu-define
  ilt-doc-submenu nil
  "Menu for Ilt-Doc"
  '("Documentation"
    ["Browse systems" ilt-doc-systems
     :help "Browse registered ASDF systems"]
    ["Browse packages" ilt-doc-packages
     :help "Browse the list of loaded packages"]
    "---"
    ["Describe symbol..." ilt-doc-symbol
     :help "Show documentation of symbol"]
    ["Describe function..." ilt-doc-function
     :help "Show documentation of function"]
    ["Describe package..." ilt-doc-package
     :help "Show package documentation"]
    ["Describe system..." ilt-doc-system
     :help "Show ASDF system documentation"]
    "---"
    [ "Lookup Documentation..." ilt-documentation-lookup t ]
    [ "Apropos..."              ilt-doc-apropos t]
    [ "Apropos all..."          ilt-doc-apropos-all t]
    [ "Apropos Package..."      ilt-doc-apropos-package t]
    [ "Apropos documentation..." ilt-doc-apropos-documentation
      :help "Search in docstrings"]
    [ "Hyperspec..."            ilt-hyperspec-lookup t ]
    "---"
    ["Quit" ilt-doc-quit
     :help "Quit ILT help"]))

(defun ilt-doc-setup-key-bindings ()
  (define-key ilt-doc-map "a" 'ilt-doc-apropos)
  (define-key ilt-doc-map "z" 'ilt-doc-apropos-all)
  (define-key ilt-doc-map "d" 'ilt-doc-symbol)
  (define-key ilt-doc-map "f" 'ilt-doc-function)
  (define-key ilt-doc-map "p" 'ilt-doc-package)
  (define-key ilt-doc-map "s" 'ilt-doc-system))

(defun ilt-doc--add-menu-to-ilt ()
  (easy-menu-add-item 'menubar-ilt nil 'ilt-doc-mode-menu))

(defun ilt-doc--replace-ilt-documentation-menu ()
  (easy-menu-add-item 'menubar-ilt nil 'ilt-doc-submenu))

(defcustom ilt-doc-parse-docstrings t
  "When enabled, docstrings are parsed and function arguments and code references are formatted accordingly."
  :type 'boolean
  :group 'ilt-doc)

(defcustom ilt-doc-lookup-in-manuals-function 'info-apropos
  "Function used to look up ilt-doc terms into manuals."
  :type 'symbol
  :group 'ilt-doc)

(provide 'ilt-doc)

;;; ilt-doc.el ends here
