Provides a nice interface to evaluating Emacs Lisp expressions.
Input is handled by the comint package, and output is passed
through the pretty-printer.

To start: M-x ilt-repl.  Type C-h m in the *ilt-repl* buffer for more info.