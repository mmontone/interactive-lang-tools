;;; ilt-inspector.el --- Tool for inspection of Emacs Lisp objects.  -*- lexical-binding: t -*-

;; Copyright (C) 2021-2022 Free Software Foundation, Inc.

;; Author: Mariano Montone <marianomontone@gmail.com>
;; URL: https://github.com/mmontone/emacs-inspector
;; Keywords: debugging, tool, emacs-lisp, development
;; Version: 0.5
;; Package-Requires: ((emacs "27") (ilt "0.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Tool for inspection language objects.

;;; Code:

(require 'eieio)
(require 'debug)
(require 'edebug)
(require 'ilt)

;;---- Utils ----------

(defun ilt-inspector--princ-to-string (object)
  "Print OBJECT to string using `princ'."
  (with-output-to-string
    (princ object)))

(defun ilt-inspector--plistp (list)
  "Return T if LIST is a property list."
  (let ((expected t))
    (and (ilt-inspector--proper-list-p list)
         (cl-evenp (length list))
         (cl-every (lambda (x)
                     (setq expected (if (eql expected t) 'symbol t))
                     (cl-typep x expected))
                   list))))

(defun ilt-inspector--alistp (list)
  "Return T if LIST is an association list."
  (and (ilt-inspector--proper-list-p list)
       (cl-every (lambda (x) (consp x)) list)))

(defun ilt-inspector--alist-to-plist (alist)
  "Convert association list ALIST to a property list."
  (let ((plist))
    (dolist (cons (reverse alist))
      (push (cdr cons) plist)
      (push (car cons) plist))
    plist))

(defun ilt-inspector--proper-list-p (val)
  "Is VAL a proper list?"
  (if (fboundp 'format-proper-list-p)
      ;; Emacs stable.
      (with-no-warnings (format-proper-list-p val))
    ;; Function was renamed in Emacs master:
    ;; http://git.savannah.gnu.org/cgit/emacs.git/commit/?id=2fde6275b69fd113e78243790bf112bbdd2fe2bf
    (with-no-warnings (proper-list-p val))))

;;--- Customization ----------------------------

(defgroup ilt-inspector nil
  "Emacs Lisp inspector customizations."
  :group 'lisp)

(defgroup ilt-inspector-faces nil
  "Emacs Lisp inspector faces."
  :group 'faces)

(defface ilt-inspector-button-face
  '((t :inherit link))
  "Face for inspector buttons."
  :group 'ilt-inspector-faces)

(defface ilt-inspector-title-face
  '((t ()))
  "Face for title describing object."
  :group 'ilt-inspector-faces)

(defface ilt-inspector-label-face
  '((t (:inherit font-lock-constant-face)))
  "Face for labels in the inspector."
  :group 'ilt-inspector-faces)

(defface ilt-inspector-value-face
  '((t (:inherit font-lock-builtin-face)))
  "Face for things which can themselves be inspected."
  :group 'ilt-inspector-faces)

(defface ilt-inspector-action-face
  '((t (:inherit font-lock-warning-face)))
  "Face for labels of inspector actions."
  :group 'ilt-inspector-faces)

(defface ilt-inspector-type-face
  '((t (:inherit font-lock-type-face)))
  "Face for type description in inspector."
  :group 'ilt-inspector-faces)

(defcustom ilt-inspector-end-column 80
  "Control print truncation size in inspector."
  :type 'integer
  :group 'ilt-inspector)

(defcustom ilt-inspector-show-documentation t
  "Whether to show variables and function documentation or not."
  :type 'boolean
  :group 'ilt-inspector)

(defcustom ilt-inspector-use-specialized-inspectors-for-lists t
  "Whether to use specialized inspectors for plists and alists."
  :type 'boolean
  :group 'ilt-inspector)

(defcustom ilt-inspector-use-font-lock-faces t
  "Use font-lock faces in inspector, instead of button faces."
  :type 'boolean
  :group 'ilt-inspector)

(defcustom ilt-inspector-slice-size 100
  "Size of sequence slices in inspector."
  :type 'integer
  :group 'ilt-inspector)

(define-button-type 'ilt-inspector-button
  'follow-link t
  'face 'ilt-inspector-button-face
  'help-echo "Inspect object")

;;-------- Inspector code -------------------

(defvar-local ilt-inspector-history nil
  "The inspector buffer history.")

(defvar-local ilt-inspector-inspected-object nil
  "The current inspected object.")

(defun ilt-inspector--insert-horizontal-line (&rest width)
  "Insert an horizontal line with width WIDTH."
  (insert (make-string (or width (window-text-width)) ?\u2500)))

(defun ilt-inspector--insert-label (label)
  "Show a LABEL in inspector buffer."
  (insert (propertize label 'face 'ilt-inspector-label-face))
  (insert ": "))

(defun ilt-inspector--insert-value (value)
  "Show a property VALUE in inspector buffer."
  (insert (propertize (ilt-inspector--princ-to-string value) 'face 'ilt-inspector-value-face)))

(defun ilt-inspector--insert-title (title)
  "Insert TITLE for inspector."
  (insert (propertize title 'face 'ilt-inspector-title-face))
  (newline)
  (ilt-inspector--insert-horizontal-line)
  (newline))

(defun ilt-inspector--print-truncated (object &optional end-column)
  "Print OBJECT to a string, truncated.
END-COLUMN controls the truncation."
  (truncate-string-to-width (prin1-to-string object)
                            (or end-column ilt-inspector-end-column)
                            nil nil t))

(cl-defgeneric ilt-inspector--face-for-object (object)
  "Return face to use for OBJECT.")

(cl-defmethod ilt-inspector--face-for-object (_object)
  "Use builtin face by default for non matching OBJECTs."
  'ilt-inspector-value-face)

(cl-defmethod ilt-inspector--face-for-object ((_ string))
  "Inspector face for STRING."
  font-lock-string-face)

(cl-defmethod ilt-inspector--face-for-object ((symbol symbol))
  "Inspector face for SYMBOLs."
  (if (keywordp symbol)
      font-lock-builtin-face
    font-lock-variable-name-face))

(cl-defmethod ilt-inspector--face-for-object ((_ integer))
  "Inspector face for INTEGERs."
  nil)

(defun ilt-inspector--insert-inspect-button (reference &optional label)
  "Insert button for inspecting OBJECT.
If LABEL has a value, then it is used as button label.
Otherwise, button label is the printed representation of OBJECT."
  (insert-button (or (and label (ilt-inspector--princ-to-string label))
                     (ilt-reference-repr reference))
                 :type 'ilt-inspector-button
                 'face (if ilt-inspector-use-font-lock-faces
                           (ilt-inspector--face-for-object reference)
                         'ilt-inspector-value-face)
                 'action (lambda (_btn)
                           (ilt-inspector-inspect reference t))
                 'follow-link t))

(defun ilt-inspector--do-with-slicer (slicer function)
  "Use SLICER and call FUNCTION on the resulting slice.
SLICE should be a function that returns a slice of some data.
FUNCTION is passed the resulting slice and a continuation function that when
called continues the consumption of slices of data, until there are no more
slices (the returned slice is nil)."
  (let ((slice (funcall slicer)))
    (when slice
      (funcall function slice
               (lambda () (ilt-inspector--do-with-slicer slicer function))))))

(defun ilt-inspector--do-with-slicer-and-more-button (slicer function)
  "Apply the SLICER function and apply FUNCTION to the resulting slice.
When FUNCTION returns non-nil, adds a [More] button that inserts the next
slice in buffer."
  (ilt-inspector--do-with-slicer
   slicer
   (lambda (slice cont)
     (let ((more-p (funcall function slice cont)))
       (when more-p
         (insert-button "[more]"
                        'action (let ((pos (point)))
                                  (lambda (_btn)
                                    (setq buffer-read-only nil)
                                    (goto-char pos)
                                    (delete-char (length "[More]"))
                                    (funcall cont)
                                    (setq buffer-read-only nil)))
                        'follow-link t))))))

;;--------- Object inspectors ----------------------------------

(cl-defgeneric ilt--inspect-object (kind object)
  "Render inspector buffer for OBJECT.

Methods of this generic function are expected to specialize on the type of
Emacs Lisp OBJECT being inspected, and write into the `current-buffer'.
The `current-buffer' is presumed to be empty.
An inspector buffer is expected to have a title, some properties and a body.
See `inspector--insert-title', `inspector--insert-label'
and `inspector--insert-value' for inserting title,and properties and its values.
For linking to another object, `inspector--insert-inspect-button'
is expected to be used.")

(cl-defmethod ilt--inspect-object (kind object)
  "Generic object inspector."
  (ilt-inspector--insert-title (ilt-reference-repr object))
  (ilt-inspector--insert-label "Type: ")
  (ilt-inspector--insert-value (ilt-reference-type object))
  (newline)
  (ilt-inspector--insert-label "Value: ")
  (ilt-inspector--insert-value (ilt-reference-repr object))
  (newline))

(cl-defmethod ilt--inspect-object ((kind (eql 'type)) object)
  "Render inspector buffer for EIEIO CLASS."
  (ilt-inspector--insert-title (format "%s type" (assoc-default 'type object)))
  (insert "direct superclasses: ")
  (dolist (superclass (eieio-class-parents class))
    (ilt-inspector--insert-inspect-button
     (eieio-class-name superclass) (eieio-class-name superclass))
    (insert " "))
  (newline)
  (insert "class slots: ")
  (dolist (slot (eieio-class-slots class))
    (insert (format "%s " (cl--slot-descriptor-name slot))))
  (newline)
  (insert "direct subclasses:")
  (dolist (subclass (eieio-class-children class))
    (ilt-inspector--insert-inspect-button
     (eieio-class-name subclass) (eieio-class-name subclass))
    (insert " ")))

(cl-defmethod ilt--inspect-object ((kind (eql 'boolean)) object)
  "Render inspector buffer for boolean T."
  (ilt-inspector--insert-title "boolean")
  (insert "value: %s" (ilt-reference-repr object)))

(cl-defmethod ilt--inspect-object ((kind (eql 'null)) object)
  "Render inspector buffer for nil object."
  (ilt-inspector--insert-title "null")
  (insert "value: %s" (ilt-reference-repr object)))

(cl-defmethod ilt--inspect-object ((kind (eql 'list)) object)
  "Inspect a list."
  (let ((buffer (current-buffer)))
    (ilt--get-reference-properties
     object '("length" "members")
     (lambda (properties)
       (let ((length (assoc-default 'length properties))
             (members (mapcar 'ilt--parse-reference (assoc-default 'members properties))))
         (with-current-buffer buffer
           (setq buffer-read-only nil)
           (ilt-inspector--insert-title "list")
           (ilt-inspector--insert-label "length")
           (insert (ilt-inspector--princ-to-string length))
           (newline 2)
           (let ((i 0)
                 (j 0))
             (ilt-inspector--do-with-slicer-and-more-button
              (lambda ()
                (when (< i length)
                  (cl-subseq members i (min (cl-incf i ilt-inspector-slice-size)
                                            length))))
              (lambda (slice _cont)
                (dolist (elem slice)
                  (insert (format "%d: " j))
                  (cl-incf j)
                  (ilt-inspector--insert-inspect-button elem)
                  (newline))
                ;; A [more] button is inserted or not depending on the boolean returned here:
                (< i length)
                )))
           (setq buffer-read-only t)))))))

(cl-defmethod ilt--inspect-object ((kind (eql 'string)) object)
  "Render inspector buffer for STRING."
  (ilt-inspector--insert-title "string")
  (prin1 (ilt-reference-repr object) (current-buffer)))

(cl-defmethod ilt--inspect-object ((kind (eql 'number)) object)
  "Render inspector buffer for NUMBER."
  (ilt-inspector--insert-title (ilt-inspector--princ-to-string (ilt-reference-type object)))
  (ilt-inspector--insert-label "value")
  (insert (ilt-inspector--princ-to-string (ilt-reference-repr object))))

(cl-defmethod ilt--inspect-object ((kind (eql 'map)) object)
  "Render inspector buffer for HASH-TABLEs."
  (let ((buffer (current-buffer)))
    (ilt--get-reference-properties
     object '("size" "count" "members")
     (lambda (properties)
       (let ((map-size (assoc-default 'size properties))
             (map-count (assoc-default 'count properties))
             (members (assoc-default 'members properties)))
	 (with-current-buffer buffer
	   (setq buffer-read-only nil)
	   (ilt-inspector--insert-title (ilt-reference-type object))
	   (ilt-inspector--insert-label "size")
	   (insert (ilt-inspector--princ-to-string map-size))
	   (newline)
	   (ilt-inspector--insert-label "count")
	   (insert (ilt-inspector--princ-to-string map-count))
	   (newline 2)
	   (if (zerop map-count)
               (insert "The map is empty.")
             (progn
               (ilt-inspector--insert-label "values")
               (newline)
               (let ((i 0))
		 (ilt-inspector--do-with-slicer-and-more-button
		  (lambda ()
                    (when (< i (length members))
                      (cl-subseq members i (min (cl-incf i ilt-inspector-slice-size)
						(length members)))))
		  (lambda (slice _cont)
                    (cl-map 'vector (lambda (elem)
                                      (ilt-inspector--insert-inspect-button
				       (ilt--parse-reference (aref elem 0)))
                                      (insert " : ")
                                      (ilt-inspector--insert-inspect-button
				       (ilt--parse-reference (aref elem 1)))
                                      (newline))
                            slice)
                    ;; Insert [more] button?
                    (< i (length members))
                    )))))))))))

(cl-defmethod ilt--inspect-object ((kind (eql 'object)) object)
  "Render inspector for objects (class instances)."
  (let ((buffer (current-buffer)))
    (ilt--get-reference-properties
     object '("members")
     (lambda (properties)
       (let ((members (assoc-default 'members properties)))
	 (with-current-buffer buffer
	   (setq buffer-read-only nil)
	   (ilt-inspector--insert-title (ilt-reference-repr object))
	   (ilt-inspector--insert-label "type")
	   (ilt-inspector--insert-value (ilt-reference-type object))
	   (newline)
	   (ilt-inspector--insert-label "slots")
           (newline)
	   (mapc
	    (lambda (slot-and-value)
	      (ilt-inspector--insert-label (prin1-to-string (car slot-and-value)))
              (insert " : ")
              (ilt-inspector--insert-inspect-button
	       (ilt--parse-reference (cdr slot-and-value)))
              (newline))
	    members)))))))

;;--- Buffers ------------------------------

(defun ilt-inspector-make-inspector-buffer ()
  "Create an inspector buffer."
  (let ((buffer (or (get-buffer "*ilt-inspector*")
                    (let ((buf (get-buffer-create "*ilt-inspector*")))
                      (with-current-buffer buf
                        (ilt-inspector-mode)
                        (make-local-variable '*))
                      buf))))
    (with-current-buffer buffer
      (setq buffer-read-only nil)
      (erase-buffer))
    buffer))

;;------ Commands -----------------------------

;;;###autoload
(defun ilt-inspect-expression (exp)
  "Evaluate EXP and inspect its result."
  (interactive "sEval and inspect: ")

  (ilt-inspector-inspect (ilt--eval-sync exp)))

(defun ilt-inspector--basic-inspect (reference)
  (defvar *)
  (let ((buffer (ilt-inspector-make-inspector-buffer)))
    (with-current-buffer buffer
      (ilt--connect-session)
      (setq ilt-inspector-inspected-object reference)
      (setq * reference)
      (ilt--inspect-object (ilt-reference-kind reference) reference)
      (goto-char 0)
      (setq buffer-read-only t)
      buffer)))

;;;###autoload
(defun ilt-inspector-inspect (reference &optional preserve-history)
  "Top-level function for inspecting OBJECTs.
When PRESERVE-HISTORY is T, inspector history is not cleared."
  (let ((current-inspected-object ilt-inspector-inspected-object)
        (buffer (ilt-inspector--basic-inspect reference)))
    (when (not preserve-history) (switch-to-buffer-other-window buffer))
    (with-current-buffer buffer
      (unless preserve-history
        (setq ilt-inspector-history nil))
      (when preserve-history
        (push current-inspected-object ilt-inspector-history)))))

(defun ilt-inspector-quit ()
  "Quit the Emacs inspector."
  (interactive)
  (setq ilt-inspector-history nil)
  (if (window-prev-buffers)
      (quit-window)
    (delete-window))
  (kill-buffer "*ilt-inspector*"))

(defun ilt-inspector-pop ()
  "Inspect previous object in inspector history."
  (interactive)
  (when ilt-inspector-history
    (let ((object (pop ilt-inspector-history)))
      (ilt-inspector--basic-inspect object))))

;;;###autoload
(defun ilt-inspect-last-sexp ()
  "Evaluate sexp before point and inspect the result."
  (interactive)
  (let ((result (eval (eval-sexp-add-defvars (elisp--preceding-sexp)) lexical-binding)))
    (ilt-inspector-inspect result)))

;;-- Inspection from Emacs debugger

;;;###autoload
(defun ilt-inspect-debugger-locals ()
  "Inspect local variables of the frame at point in debugger backtrace."
  (interactive)
  (let* ((nframe (debugger-frame-number))
         (locals (backtrace--locals nframe)))
    (ilt-inspector-inspect (ilt-inspector--alist-to-plist locals))))

;;;###autoload
(defun ilt-inspect-debugger-local (varname)
  "Inspect local variable named VARNAME of frame at point in debugger backtrace."
  (interactive
   (list
    (completing-read "Inspect local variable: "
                     (with-current-buffer "*Backtrace*"
                       ;; The addition of 0 to the return value of (debugger-frame-number) is necessary here. Why?? Ugly hack ...
                       ;; On Emacs 29.0.50 with native comp at least ..
                       (let ((n (+ (debugger-frame-number) 0)))
                         (mapcar #'car (backtrace--locals n)))))))
  (with-current-buffer "*Backtrace*"
    (let* ((n (debugger-frame-number))
           (locals (backtrace--locals n)))
      (ilt-inspector-inspect (cdr (assoc (intern varname) locals))))))

;;;###autoload
(defun ilt-inspect-debugger-current-frame ()
  "Inspect current frame in debugger backtrace."
  (interactive)
  (let* ((nframe (debugger-frame-number))
         (frame (backtrace-frame nframe)))
    (ilt-inspector-inspect frame)))

;;;###autoload
(defun ilt-inspect-debugger-frame-and-locals ()
  "Inspect current frame and locals in debugger backtrace."
  (interactive)
  (let* ((nframe (debugger-frame-number))
         (locals (backtrace--locals nframe))
         (frame (backtrace-frame nframe)))
    (ilt-inspector-inspect (list :frame frame
                                 :locals (ilt-inspector--alist-to-plist locals)))))

;;--------- Inspector mode ---------------------------------

(defvar ilt-inspector-mode-map
  (let ((map (make-keymap)))
    (define-key map "q" #'ilt-inspector-quit)
    (define-key map "l" #'ilt-inspector-pop)
    (define-key map "e" #'eval-expression)
    (define-key map "n" #'forward-button)
    (define-key map "p" #'backward-button)
    map))

(easy-menu-define
  ilt-inspector-mode-menu ilt-inspector-mode-map
  "Menu for inspector."
  '("ILT Inspector"
    ["Previous" ilt-inspector-pop :help "Inpect previous object"]
    ["Evaluate" eval-expression :help "Evaluate expression with current inspected object as context"]
    ["Exit" ilt-inspector-quit :help "Quit the Emacs Lisp inspector"]))

(defvar ilt-inspector-tool-bar-map
  (let ((map (make-sparse-keymap)))
    (tool-bar-local-item-from-menu
     'ilt-inspector-pop "left-arrow" map ilt-inspector-mode-map
     :rtl "left-arrow"
     :label "Back"
     :vert-only t)
    (tool-bar-local-item-from-menu
     'ilt-inspector-quit "exit" map ilt-inspector-mode-map
     :vert-only t)
    map))

(define-derived-mode ilt-inspector-mode fundamental-mode "Ilt-Inspector"
  "Major mode for the Emacs Lisp Inspector."
  (setq-local tool-bar-map ilt-inspector-tool-bar-map))

(provide 'ilt-inspector)

;;; ilt-inspector.el ends here
