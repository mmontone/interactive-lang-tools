### inline-message

Show messages inlined in buffers

- **Version:** 0.1
- **Author:** Artur Malabarba <bruce.connor.am@gmail.com>
- **Keywords:** 
- **Reqs:** 

### ilt-jscl

Interactive Language Tools backend for JSCL

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** 
- **Reqs:** emacs v25, ilt v0.1

### phel-mode

Major mode for editing Phel language source files

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** tools, editing, php, phel
- **Reqs:** emacs v25, clojure-mode v5.15.1

### ilt-phel

Interactive Language Tools backend for Phel language

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** 
- **Reqs:** emacs v25, ilt v0.1, phel-mode v0.1

### ilt-doc

Documentation buffers for ILT definitions

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** help, lisp, ilt, common-lisp
- **Reqs:** emacs v25, ilt v0.1

### ilt-inspector

Tool for inspection of Emacs Lisp objects.

- **Version:** 0.5
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** debugging, tool, emacs-lisp, development
- **Reqs:** emacs v27, ilt v0.1

### ilt-repl

interaction mode for Emacs Lisp

- **Version:** 0.1
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** lisp
- **Reqs:** ilt v0.1

### ilt

Interactive Language Tools

- **Version:** 0.3
- **Author:** Mariano Montone <marianomontone@gmail.com>
- **Keywords:** help, lisp, slime, common-lisp
- **Reqs:** emacs v25, sesman v0.3.3, websocket v1.13, anaphora v1.0.4, inline-message v0.1, request v0.3.3

