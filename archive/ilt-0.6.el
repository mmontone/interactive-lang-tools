;;; ilt.el --- Interactive Language Tools    -*- lexical-binding: t -*-

;; Copyright (C) 2022 Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; URL: https://github.com/mmontone/interactive-lang-tools
;; Keywords: help, lisp, slime, common-lisp
;; Version: 0.6
;; Package-Requires: ((emacs "25")(sesman "0.3.3")(websocket "1.13")(anaphora "1.0.4")(inline-message "0.1")(request "0.3.3")(s "1.13.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <http://www.gnu.org/licenses/>.

;;; Commentary:

;; Tools for interactive programming languages.

;;; Code:

(require 'mule-util)
(require 'request)
(require 'anaphora)
(require 'websocket)
(require 'inline-message)
(require 'json)
(require 'sesman)
(require 's)

(defvar ilt-backends '()
  "The list of available ILT backends.")

(defvar ilt--lisp-modes '(lisp-mode emacs-lisp-mode
                                    lisp-interaction-mode
                                    slime-mode
                                    slime-repl-mode)
  "This is used by ILT mode to determine the last expression, etc.")

(defvar ilt-mode-backend-types nil
  "An alist for assigning default ILT backend types to Emacs modes.")

(cl-defstruct ilt-connection
  "An ILT connection."
  id
  backend
  evaluations
  options)

(cl-defgeneric ilt--close-connection (ilt-connection)
  (:documentation "Close the ILT-CONNECTION."))

;; ---- Connection session handling -------------------------------

(cl-defmethod sesman-start-session ((_system (eql 'ILT)))
  "Start an ILT session."
  (list "ILT"))

(cl-defmethod sesman-quit-session ((_system (eql 'ILT)) session)
  "Quit a ILT SESSION."
  (mapc #'ilt--close-connection (cdr session))
  ;; if there are no more session we can kill all ancillary buffers
  ;;(unless (ilt-sessions)
  ;;  (ilt-close-ancillary-buffers))
  )

(defun ilt--init-session (ilt-connection)
  (setq-local sesman-system 'ILT)
  (let ((session (sesman-start-session 'ILT)))
    (sesman-register 'ILT session)
    (sesman-add-object 'ILT "ILT" ilt-connection)))

;; --------------- Generic connection protocol ---------------------------------

(cl-defun ilt-connection (&optional (error-p t))
  "Return the current ILT connection.
Signals error if ERROR-P is true and there's no connection."
  (or (second (sesman-current-session 'ILT))
      (and error-p (error "Not connected"))))

(cl-defgeneric ilt--connect-backend (backend &rest args)
  "Generic function for connecting to the ILT BACKEND.
ARGS are used to initialize the backend.")

(cl-defmethod ilt--connect-backend (backend &rest _args)
  "Signal an error if BACKEND cannot be initialized."
  (error "`ilt--connect-backend' not implemented for backend: %s" backend))

(cl-defun ilt-connect (backend &rest args &key _connection-type &allow-other-keys)
  "Start an ILT connection using BACKEND.
ARGS are passed as initialization parameters to backend connection."
  (interactive (list (or (and (not prefix-arg)
			      (assoc-default major-mode ilt-mode-backend-types))
                         (intern (completing-read "Backend: " ilt-backends)))))
  (let ((ilt-connection (apply #'ilt--connect-backend backend args)))
    (ilt--init-backend backend ilt-connection)
    (ilt--open-connection ilt-connection)
    (ilt--init-session ilt-connection)))

(cl-defgeneric ilt--init-backend (backend ilt-connection)
  "Initialize ILT BACKEND from ILT-CONNECTION.")

(cl-defgeneric ilt--open-connection (ilt-connection)
  "Initialize the ILT-CONNECTION.")

(cl-defgeneric ilt--find-buffer-module (ilt-backend)
  "Extract the module name from current buffer for ILT-BACKEND.
Used for evaluation calls.")

(cl-defmethod ilt--find-buffer-module ((_ilt-backend t))
  "Default buffer module is none."
  nil)

(cl-defgeneric ilt--thing-at-point (ilt-backend thing no-properties)
  "Get thing-at-point for ILT-BACKEND.")

(cl-defmethod ilt--thing-at-point (_ilt-backend thing no-properties)
  (thing-at-point thing no-properties))

;;---------- Connection types ---------------------------------------------

;;---- HTTP connection --------------------------------------------

(cl-defstruct (http-ilt-connection (:include ilt-connection))
  "HTTP ILT connection."
  url)

(cl-defmethod ilt--open-connection ((connection http-ilt-connection))
  (ignore connection)
  ;; nothing to do
  )

(cl-defmethod ilt--close-connection ((connection http-ilt-connection))
  (ignore connection)
  ;; nothing to do
  )


;;--- Websocket connection ----------------------------------

(cl-defstruct (ilt--websocket-connection (:include ilt-connection))
  "A Websocket connection type."
  server
  port
  clients
  (callbacks (make-hash-table)))

(cl-defmethod ilt--open-connection ((connection ilt--websocket-connection))
  (setf (ilt--websocket-connection-server connection)
        (websocket-server
         (ilt--websocket-connection-port connection)
         :host 'local
         :on-message (lambda (websocket frame)
                       (ilt--websocket-receive-message websocket frame connection))
         :on-open (lambda (websocket)
                    (push websocket (ilt--websocket-connection-clients connection))
                    (message "websocket opened"))
         :on-close (lambda (websocket)
                     (setf (ilt--websocket-connection-clients connection)
                           (delete websocket (ilt--websocket-connection-clients connection)))
                     (message "websocket closed")))))

(cl-defmethod ilt--close-connection ((connection ilt--websocket-connection))
  (websocket-close (ilt--websocket-connection-server connection)))

;;---------- JSONRPC calls ------------------------------------------------

(defvar ilt--jsonrpc-id 0
  "ID counter for JSONRPC request objects.")

(defun ilt--jsonrpc-error-message (error)
  "Extract error message from JSONRPC ERROR."
  (if (= (assoc-default 'code error) -32000)
      ;; Server error
      ;; Reserved for implementation-defined server-errors
      (or (assoc-default 'message (assoc-default 'data error))
          (assoc-default 'message error))
    (assoc-default 'message error)))

(defun ilt--show-jsonrpc-error (error)
  "Show JSONRPC ERROR to user."
  (ilt--show-error-message (ilt--jsonrpc-error-message error)))

(cl-defun ilt--call-method (method args &key success error)
  "Call METHOD with ARGS.
SUCCESS and ERROR are callbacks."
  (ilt--connection-call-method (ilt-connection)
                               method args
                               :success success
                               :error error))

(cl-defun ilt--call-method-sync (method args)
  "Synchronous call METHOD with ARGS."
  (let (result err)
    (ilt--call-method method args
                      :success (lambda (jsonrpc-result &optional _)
                                 (setq result jsonrpc-result))
                      :error (lambda (jsonrpc-err &optional _data)
                               (setq err jsonrpc-err)))

    (while (not (or result err))
      (sleep-for 0.1))

    (if err
        (progn
          (ilt--show-jsonrpc-error err)
          (error (ilt--jsonrpc-error-message err)))
      result)))

(cl-defgeneric ilt--connection-call-method (ilt-connection method args &key success error)
  (:documentation "Call JSONRPC METHOD via ILT-CONNECTION."))

(defun ilt--encode-jsonrpc-request (method params &optional id)
  (list (cons 'id (or id (cl-incf ilt--jsonrpc-id)))
        (cons 'method method)
        (cons 'params params)
        (cons 'jsonrpc "2.0")))

(cl-defmethod ilt--connection-call-method ((connection http-ilt-connection) method args &key success error)
  "Call JSON RPC method."
  (request (or (and connection (http-ilt-connection-url connection))
               (error "ILT: Not connected"))
    :type "POST"
    :parser 'json-read
    :data (json-encode-alist (ilt--encode-jsonrpc-request method args))
    :headers '(("Content-Type" . "application/json"))
    :success (cl-function
              (lambda (&key data &allow-other-keys)
                (let ((result (when (assoc-default 'result data)
                                (assoc-default 'result data)))
                      (err (assoc-default 'error data)))
                  (when result
                    (funcall success result data))
                  (when err
                    (if error
                        (funcall error err data)
                      (ilt--show-jsonrpc-error err))))))
    :error (lambda (&rest _args)
             (debug args))))

(defun ilt--websocket-receive-message (_websocket frame connection)
  (let* ((jsonrpc-response (json-read-from-string (websocket-frame-payload frame))))
    (let ((callback (or (gethash (assoc-default 'id jsonrpc-response)
                                 (ilt--websocket-connection-callbacks connection))
                        (error "Callback not found: %s" (assoc-default 'id jsonrpc-response)))))
      (funcall callback jsonrpc-response))))

(cl-defmethod ilt--connection-call-method
  ((connection ilt--websocket-connection) method args &key success error)

  (dolist (websocket-client (ilt--websocket-connection-clients connection))
    (let* ((jsonrpc-message (ilt--encode-jsonrpc-request method args))
           (callback (lambda (jsonrpc-response)
                       (let* ((result (assoc-default 'result jsonrpc-response))
                              (err (assoc-default 'error jsonrpc-response)))
                         (when result
                           (funcall success result))
                         (when err
                           (if error
                               (funcall error err)
                             (ilt--show-jsonrpc-error err)))))))
      (setf (gethash (cl-the integer (assoc-default 'id jsonrpc-message))
                     (ilt--websocket-connection-callbacks connection))
            callback)
      (websocket-send-text websocket-client
                           (json-encode-alist jsonrpc-message)))))

;;---------- ILT protocol --------------------------

(defun ilt--call-backend-function (func &rest args)
  "Call backend function FUNC with ARGS.
A 'backend function' is a generic function that specializes the
connection backend on its first parameter.
FUNC is called using the current ILT connection backend as specializer.
This is an utility function to make the calls to those
kind of functions simpler and shorter."
  (apply func (ilt-connection-backend (ilt-connection)) args))

;;---------- References ----------------------------

(cl-defstruct ilt-reference
  id name repr type kind doc properties)

(defun ilt--parse-reference (json)
  (make-ilt-reference
   :id (assoc-default 'id json)
   :name (assoc-default 'name json)
   :repr (assoc-default 'repr json)
   :type (assoc-default 'type json)
   :kind (intern (assoc-default 'kind json))
   :doc (assoc-default 'doc json)
   :properties (assoc-default 'properties json)))

;;---------- Definitions ---------------------------

(defclass ilt-definition ()
  ((id :accessor ilt--definition-id
       :initarg :id)
   (name :accessor ilt--definition-name
         :initarg :name)
   (documentation :accessor ilt--definition-doc
                  :initarg :doc)
   (location :accessor ilt--definition-location
             :initarg :location)
   (type :initarg :type
         :accessor ilt--definition-type)))

(defclass ilt-module-member (ilt-definition)
  ((module :initarg :module
           :accessor ilt--module)))

(defclass ilt-variable (ilt-module-member)
  ())

(defclass ilt-function (ilt-module-member)
  ((args :accessor ilt--function-args :initarg :args
         :initform nil)))

(defclass ilt-type (ilt-module-member)
  ((class :initarg :class
          :accessor ilt--type-class)))

(defclass ilt-module (ilt-definition)
  ((members :initarg :members
            :accessor ilt--module-members)
   (exports :initarg :exports
            :accessor ilt--module-exports)))

(defun ilt--parse-definition (json)
  (cond
   ((string= (assoc-default 'kind json) "module")
    (make-instance 'ilt-module
                   :id (assoc-default 'id json)
                   :name (assoc-default 'name json)
                   :doc (assoc-default 'doc json)
                   :location (assoc-default 'location json)
                   :type (assoc-default 'type json)
                   :members (assoc-default 'members json)))
   ((string= (assoc-default 'kind json) "type")
    (make-instance 'ilt-type
                   :id (assoc-default 'id json)
                   :name (assoc-default 'name json)
                   :doc (assoc-default 'doc json)
                   :module (assoc-default 'module json)
                   :class (assoc-default 'class json)
                   :location (assoc-default 'location json)
                   :type (assoc-default 'type json)))
   ((string= (assoc-default 'kind json) "function")
    (make-instance 'ilt-function
                   :id (assoc-default 'id json)
                   :name (assoc-default 'name json)
                   :doc (assoc-default 'doc json)
                   :module (assoc-default 'module json)
                   :location (assoc-default 'location json)
                   :type (assoc-default 'type json)
		   :args (assoc-default 'args json)))
   ((string= (assoc-default 'kind json) "variable")
    (make-instance 'ilt-variable
                   :id (assoc-default 'id json)
                   :name (assoc-default 'name json)
                   :doc (assoc-default 'doc json)
                   :module (assoc-default 'module json)
                   :location (assoc-default 'location json)
                   :type (assoc-default 'type json)))
   (t (error "Error parsing definition: %s" json))))

;;--------- Commands ---------------------

(defun ilt--get-definition-properties (def properties success)
  (ilt--call-method "get_definition_properties"
                    (list
                     (ilt--definition-id def)
                     properties)
                    :success (lambda (properties &optional _)
                               (funcall success properties))))

(defun ilt--get-reference-properties (ref properties success)
  (ilt--call-method "get_reference_properties"
                    (list
                     (if (typep ref 'ilt-reference)
                         (ilt-reference-id ref)
                       ref)
                     properties)
                    :success (lambda (properties &optional _)
                               (funcall success properties))))

(defun ilt--show-evaluation-result (result-string)
  (let ((message (concat  "=> " result-string)))
    (inline-message message)
    (display-message-or-buffer message)))

(defun ilt--show-error-message (message)
  (inline-message message :prepend-face 'error)
  (message message))

(defun ilt-eval-region (start end)
  (interactive "r")

  ;; If there's no region selected, then select region using `mark-paragraph`
  (when (= start end)
    (mark-paragraph)
    (setq start (point))
    (setq end (mark)))

  (let* ((ilt-connection (ilt-connection))
         (source-code (buffer-substring start end))
         (module (ilt--call-backend-function #'ilt--find-buffer-module)))
    (ilt--call-method "evaluate" (list source-code module (ilt-connection-id ilt-connection))
                      :success
                      (lambda (res &optional _)
                        (ilt--show-evaluation-result (assoc-default 'repr res))))))

(defun ilt--last-expr ()
  (if (member major-mode ilt--lisp-modes)
      (buffer-substring-no-properties
       (save-excursion (backward-sexp) (point))
       (point))
    ;; else, use mark-paragraph
    (progn
      (mark-paragraph)
      (let* ((start (point))
             (end (mark)))
        (buffer-substring start end)))))

(defun ilt--region-for-definition-at-point ()
  (if (member major-mode ilt--lisp-modes)
      (save-excursion
        (save-match-data
          (end-of-defun)
          (let ((end (point)))
            (beginning-of-defun)
            (list (point) end))))
    ;; else
    (error "TODO: dispatch on ILT backend")))

(cl-defun ilt-eval-last-expr (&optional ctxid)
  "Evaluate last expression at point."
  (interactive)

  (ilt--call-method "evaluate"
                    (list (ilt--last-expr)
                          (ilt--call-backend-function #'ilt--find-buffer-module)
                          (or ctxid (ilt-connection-id (ilt-connection))))
                    :success
                    (lambda (res &optional _)
                      (ilt--show-evaluation-result (assoc-default 'repr res)))))

(cl-defun ilt-eval (expr &optional ctxid)
  "Evaluate EXPR in context with id CTXID."
  (interactive "sEvaluate: ")
  (ilt--call-method "evaluate"
                    (list expr
                          (ilt--find-buffer-module (ilt-connection-backend (ilt-connection)))
                          (or ctxid (ilt-connection-id (ilt-connection))))
                    :success
                    (lambda (res &optional _)
                      (ilt--show-evaluation-result (assoc-default 'repr res)))))

(defun ilt--eval-sync (expr)
  (ilt--call-method-sync "evaluate"
                         (list expr
                               (ilt--find-buffer-module (ilt-connection-backend (ilt-connection)))
                               (ilt-connection-id (ilt-connection)))))

(defun ilt-compile-region (start end)
  (interactive "r")

  ;; If there's no region selected, then select region using `mark-paragraph`
  (when (= start end)
    (mark-paragraph)
    (setq start (point))
    (setq end (mark)))

  (let ((source-code (buffer-substring start end)))
    (ilt--call-method "compile" (list source-code
                                      (ilt--find-buffer-module (ilt-connection-backend (ilt-connection))))
                      :success (lambda (_ &optional _)
                                 (message "Compilation done.")))))

(defun ilt-compile-last-expr ()
  (interactive)

  (ilt--call-method "compile" (list (ilt--last-expr)
                                    (ilt--find-buffer-module (ilt-connection-backend (ilt-connection))))
                    :success (lambda (_ &optional _)
                               (message "Compilation done"))))

(defun ilt-compile-definition ()
  (interactive)
  (apply #'ilt-compile-region (ilt--region-for-definition-at-point)))


(defun ilt-load-file (file)
  (interactive)
  (ilt--call-method "load_file" (list (or file buffer-file-name))
                    :success (lambda (_ &optional _)
                               (message "File loaded."))))

(defun ilt-load-buffer ()
  (interactive))

(defun ilt-compile-file ()
  (interactive)
  (ilt--call-method "compile_file" (list buffer-file-name)
                    :success (lambda (_ &optional _)
                               (message "File compiled."))))

(defun ilt-compile-buffer ()
  (interactive))

(defun ilt-inspect-region (start end)
  (interactive "r")

  ;; If there's no region selected, then select region using `mark-paragraph`
  (when (= start end)
    (mark-paragraph)
    (setq start (point))
    (setq end (mark)))

  (let ((source-code (buffer-substring start end)))
    (ilt--call-method "evaluate" (list source-code
                                       (ilt--find-buffer-module (ilt-connection-backend (ilt-connection)))
                                       (ilt-connection-id (ilt-connection)))
                      :success
                      (lambda (res &optional _)
                        (ilt-inspector-inspect (ilt--parse-reference res))))))

(defun ilt-inspect-expr (expr)
  "Evaluate EXPR and inspect the result."
  (interactive "sInspect expression: ")
  (ilt--call-method "evaluate" (list expr
                                     (ilt--find-buffer-module (ilt-connection-backend (ilt-connection)))
                                     (ilt-connection-id (ilt-connection)))
                    :success (lambda (res &optional _)
                               (ilt-inspector-inspect (ilt--parse-reference res)))))

(defun ilt-inspect-last-expr ()
  (interactive)

  (ilt-inspect-expr (ilt--last-expr)))

(cl-defgeneric ilt--describe-definition% (def)
  (:documentation "Describe definition DEF.
Specialized on ILT-DEFINITION types."))

;; Generic
(cl-defmethod ilt--describe-definition% ((def ilt-definition))
  "Default implementation for describing definitions."
  (insert (ilt--definition-name def))
  (insert " - ")
  (insert (ilt--definition-type def))
  (newline 2)
  (awhen (ilt--definition-doc def)
    (insert it)))

(cl-defmethod ilt--describe-definition% ((def ilt-function))
  "Describe function."
  (insert (ilt--definition-name def))
  (insert " - ")
  (insert (ilt--definition-type def))
  (newline 2)
  (awhen (ilt--function-args def)
    (insert "args: ")
    (insert (ilt--format-arglist it))
    (newline 2))
  (awhen (ilt--definition-doc def)
    (insert it)))

(cl-defmethod ilt--describe-definition% ((module ilt-module))
  "Describe an ILT module."
  (cl-call-next-method)
  (let ((buffer (current-buffer)))
    (ilt--get-definition-properties
     module (list "members")
     (lambda (properties)
       (with-current-buffer buffer
         (setq buffer-read-only nil)
         (ilt--insert-definitions-list
          (map 'vector #'ilt--parse-definition (assoc-default 'members properties))
          #'ilt--describe-definition)
         (setq buffer-read-only t))))))

(defun ilt--describe-definition (def)
  "Describe definition DEF."
  (let ((buffer-name (format "Definiton: %s (%s)"
                             (ilt--definition-name def)
                             (ilt--definition-type def))))
    (let ((buffer (get-buffer-create buffer-name)))
      (with-current-buffer buffer
        (ilt--connect-session)
        (ilt--describe-definition% def)
        (setq buffer-read-only t)
        (local-set-key (kbd "q") 'kill-current-buffer)
        (switch-to-buffer buffer)
        (display-buffer buffer)
        (scroll-up)
        (goto-char (point-min))))))

(defun ilt--insert-definitions-list (definitions action)
  (mapc (lambda (def)
          (insert-button
           (if (typep def 'ilt-module)
               (format "%s (%s)" (ilt--definition-name def)
                       (ilt--definition-type def))
             (format "[%s] %s (%s)"
                     (ilt--module def)
                     (ilt--definition-name def)
                     (ilt--definition-type def)))
           'face 'apropos-symbol
           'action (lambda (_)
                     (funcall action def))
           'follow-link t)
          (newline)
          (if (and (ilt--definition-doc def)
                   (not (zerop (length (ilt--definition-doc def)))))
              (insert (truncate-string-to-width
                       (remove ?\n (ilt--definition-doc def))
                       70 0 nil "..."))
            (insert "Not documented"))
          (newline))
        ;; Why? This shouldn't happen
        (remove nil definitions)))

(defun ilt--open-apropos-buffer (what definitions)
  (let ((buffer (generate-new-buffer (format "*ilt-apropos: %s*" what))))
    (with-current-buffer buffer
      (setq-local sesman-system 'ILT)
      (ilt--insert-definitions-list definitions #'ilt--describe-definition)
      (switch-to-buffer buffer)
      (setq buffer-read-only t)
      (goto-char (point-min)))))

(defun ilt-apropos (what)
  (interactive "sApropos: ")
  (ilt--call-method "apropos" (list what)
                    :success (lambda (res &optional _)
                               (ilt--open-apropos-buffer what (mapcar #'ilt--parse-definition res)))))

(defun ilt--open-definitions-buffer (buffer-name definitions action)
  (let ((buffer (generate-new-buffer buffer-name)))
    (with-current-buffer buffer
      (setq-local sesman-system 'ILT)
      (ilt--insert-definitions-list definitions action)
      (setq buffer-read-only t)
      (local-set-key (kbd "q") 'kill-current-buffer)
      (switch-to-buffer buffer)
      (display-buffer buffer)
      (scroll-up)
      (goto-char (point-min)))))

(defun ilt-list-modules ()
  (interactive)
  (ilt--call-method "list_modules" ()
                    :success (lambda (defs &optional _)
                               (let ((modules (mapcar #'ilt--parse-definition defs)))
                                 (ilt--open-definitions-buffer "*ILT Modules*" modules #'ilt--describe-definition)))))

(defun ilt-apropos-module (module-name)
  (interactive (list (completing-read "Module: "
                                      (mapcar (lambda (m)
                                                (assoc-default 'name m))
                                              (ilt--call-method-sync "list_modules" ())))))
  (ilt--describe-definition (make-instance 'ilt-module :name module-name
                                           :type "module")))

;; The programming mode is in charge of providing support
;; for correct thing-at-point. See: thing-at-point-provider-alist

(defun ilt-describe-thing-at-point (thing)
  "Describe THING at point."

  (interactive (list (or (ilt--thing-at-point 'symbol 'no-properties)
                         (read-string "Describe: "))))

  (when thing
    (ilt--call-method
     "find_definitions" (list thing (ilt--find-buffer-module (ilt-connection-backend (ilt-connection))))
     :success (lambda (defs &optional _)
                (when defs
                  (map 'vector
                       (lambda (def)
                         (ilt--describe-definition (ilt--parse-definition def)))
                       defs))))))

(defun ilt--read-module-name ()
  "Read the name of an ILT module."
  (let ((modules (mapcar #'ilt--parse-definition (ilt--call-method-sync "list_modules" ()))))
    (completing-read "Module: " (mapcar #'ilt--definition-name modules))))

(defun ilt-describe-module (module-name)
  "Describe module named with MODULE-NAME."
  (interactive (list (ilt--read-module-name)))

  (let* ((modules (mapcar #'ilt--parse-definition (ilt--call-method-sync "list_modules" ())))
         (module (cl-find module-name modules :key #'ilt--definition-name :test #'string=)))
    (when (not module)
      (error "Module not found: %s" module-name))
    (ilt--describe-definition module)))


;; completion-in-region mode calls completion function several times,
;; even when just scrolling the list of completions. I don't know how to
;; disable that. So, we use a completion cache for now ...

(defvar ilt--completions-cache (make-hash-table :test 'equal))

(cl-defgeneric ilt--what-to-complete (backend)
  "Returns the start, end, and string at point to be completed.
Sent to the backend.")

(cl-defmethod ilt--what-to-complete (_backend)
  "The default implementation uses `bounds-of-thing-at-point'."
  (let* ((bounds (bounds-of-thing-at-point 'symbol))
         (start (car bounds))
         (end (cdr bounds)))
    (list start end (and start end (buffer-substring start end)))))

(cl-defun ilt-completion-at-point ()
  "Complete thing at point.
Calls the `complete' jsonrpc method.

Returns (START END COLLECTION . PROPS), where:
 START and END delimit the entity to complete and should include point,
 COLLECTION is the completion table to use to complete the entity, and
 PROPS is a property list for additional information."
  (let ((ilt-connection (ilt-connection)))
    (cl-destructuring-bind (start end what-to-complete)
	(ilt--what-to-complete (ilt-connection-backend ilt-connection))

      ;; Try cache first
      (awhen (gethash (cons ilt-connection what-to-complete) ilt--completions-cache)
	(cl-return-from ilt-completion-at-point it))

      (let ((completions (list start end
                               (cl-coerce
				(ilt--call-method-sync
				 "complete"
				 (list what-to-complete))
				'list))))
	(setf (gethash (cons ilt-connection what-to-complete) ilt--completions-cache) completions)
	completions))))

(defun ilt--fnsym-in-current-expr ()
  "Return a list of current function name and argument index."
  (save-excursion
    (unless (nth 8 (syntax-ppss))
      (let ((argument-index (1- (elisp--beginning-of-sexp))))
        ;; If we are at the beginning of function name, this will be -1.
        (when (< argument-index 0)
          (setq argument-index 0))
        (list (ilt--call-backend-function #'ilt--thing-at-point 'symbol 'no-properties) argument-index)))))

(defun ilt--format-arglist (argslist)
  "Format function ARGSLIST."
  (cond
   ((stringp argslist)
    argslist)
   ((sequencep argslist)
    (concat "("
	    (s-join ", " (mapcar (lambda (arg)
				   (if (assoc-default 'type arg)
				     (format "%s:<%s>" (assoc-default 'name arg)
					     (assoc-default 'type arg))
				   (assoc-default 'name arg)))
			       argslist))
	    ")"))
   (t (error "Invalid arglist: %s" argslist))))

(defun ilt--eldoc-funcall (callback &rest _ignored)
  "eldoc docstring for function calls."
  (let* ((sym-info (ilt--fnsym-in-current-expr))
         (fname (car sym-info)))
    (when fname
      (ilt--call-method
       "find_definitions" (list fname
				(ilt--find-buffer-module (ilt-connection-backend (ilt-connection)))
				"function")
       :success (lambda (defs &optional _)
		  (when (not (zerop (length defs)))
		    (let ((func (ilt--parse-definition (aref defs 0))))
		      (funcall callback (format "%s: %s"
						(ilt--definition-name func)
						(let ((args (ilt--function-args func)))
						  (ilt--format-arglist args)))))))))))

(defmacro define-ilt-backend (name options &rest body)
  "Definition macro for ILT backends."
  `(progn
     ;; Custom connection command
     (defun ,(intern (concat (symbol-name name) "-connect")) (&rest args)
       (interactive)
       (apply #'ilt-connect
	      ',(or (cl-getf options :backend)
		    name)
	      ,@(cl-getf options :backend-options)
	      args))
     ;; Custom repl command
     (defun ,(intern (concat (symbol-name name) "-repl")) ()
       (interactive)
       (ilt-repl))
     ;; Register as backend
     (cl-pushnew ',name ilt-backends)
     ;; Register as lisp mode
     ,(when (cl-getf options :lisp-mode)
	`(cl-pushnew ',(cl-getf options :mode) ilt--lisp-modes))
     ;; Bind backend type to mode
     ,(when (cl-getf options :mode-default-backend?)
	`(cl-pushnew '(,(cl-getf options :mode) . ,name)
		     ilt-mode-backend-types
		     :test 'cl-equalp))
     ;; Backend initialization method
     (cl-defmethod ilt--init-backend ((_backend (eql ',name)) connection)
       ,@body)
     ))

(defvar ilt-mode-map
  (let ((map (make-keymap)))
    (define-key map (kbd "C-c C-c") 'ilt-compile-definition)
    (define-key map (kbd "C-c C-r") 'ilt-compile-region)
    (define-key map (kbd "C-x C-e") 'ilt-eval-last-expr)
    (define-key map (kbd "C-x C-I") 'ilt-inspect-last-expr)
    (define-key map (kbd "C-x C-r") 'ilt-eval-region)
    (define-key map (kbd "C-c M-k") 'ilt-compile-file)
    (define-key map (kbd "C-C C-l") 'ilt-load-file)
    (define-key map (kbd "C-c M-i") 'ilt-complete)
    (define-key map (kbd "C-c C-d") 'ilt-describe-thing-at-point)
    map))

(easy-menu-define
  ilt-mode-menu ilt-mode-map
  "Menu for Interactive Language Tools"
  '("ILT"
    ["Connect ..." ilt-start]
    ["Browse modules" ilt-list-modules
     :help "Browse modules"]
    "---"
    ["Evaluate" ilt-eval-last-expr]
    ["Evaluate region" ilt-eval-region]
    ["Compile" ilt-compile-last-expr]
    ["Compile region" ilt-compile-region]
    ["Compile definition" ilt-compile-definition]
    ["Load file" ilt-load-file]
    ["Compile file" ilt-compile-file]
    ["Inspect" ilt-inspect-last-expr]
    ["Inspect region" ilt-inspect-region]
    "---"
    ["Describe ..." ilt-describe-thing-at-point
     :help "Show documentation of thing at point"]
    ["Describe function..." ilt-describe-function
     :help "Show documentation of function"]
    ["Describe module..." ilt-describe-module
     :help "Show module documentation"]
    "---"
    ["Lookup Documentation..." ilt-documentation-lookup t]
    ["Apropos..."              ilt-apropos t]
    ["Apropos all..."          ilt-apropos-all t]
    ["Apropos module..."       ilt-apropos-module t]
    ["Apropos documentation..." ilt-apropos-documentation :help "Search in docstrings"]))

(defvar ilt-connect-on-start t
  "When enabled, try to stablish an ILT connection when ILT mode is enabled.")

(defun ilt--connect-session ()
  "Connect ILT to current buffer."
  (setq-local sesman-system 'ILT)
  ;; Look for an existing session
  (let ((sessions (sesman-sessions 'ILT)))
    (cond
     ((null sessions)
      (when ilt-connect-on-start
        (call-interactively #'ilt-connect)))
     ((= (length sessions) 1)
      (sesman-link-with-buffer (current-buffer) (first sessions)))
     ((> (length sessions) 1)
      ;; Let the user choose the session
      (sesman-link-with-buffer (current-buffer))))))

(define-minor-mode ilt-mode "Interactive Language Mode"
  :lighter " ILT"
  :keymap ilt-mode-map
  :group 'ilt

  ;; sessions
  (require 'sesman)
  (sesman-install-menu ilt-mode-map)
  (add-hook 'ilt-mode-hook #'ilt--connect-session)

  ;; completion
  (add-hook 'completion-at-point-functions #'ilt-completion-at-point nil 'local)

  ;; eldoc
  (add-hook 'eldoc-documentation-functions #'ilt--eldoc-funcall nil t)
  (eldoc-mode)
  )

(provide 'ilt)

;;; ilt.el ends here
