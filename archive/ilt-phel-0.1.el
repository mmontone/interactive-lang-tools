;;; ilt-phel.el --- Interactive Language Tools backend for Phel language  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Version: 0.1
;; Package-Requires: ((emacs "25") (ilt "0.1") (phel-mode "0.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Interactive Language Tools backend for Phel language

;;; Code:

(require 'ilt)
(require 'phel-mode)

(cl-defmethod ilt--connect-backend ((backend (eql 'phel)) &rest args)
  (apply #'make-http-ilt-connection
	 :url (or (cl-getf args :url)
		  (read-string "Url: "))
	 :backend 'phel
	 args))

(defconst phel-namespace-regexp
  (rx line-start "(" (? "phel.core/") (or "in-ns" "ns" "ns+") symbol-end))

(defun phel--find-ns-in-direction (direction)
  "Return the nearest namespace in a specific DIRECTION.
DIRECTION is `forward' or `backward'."
  (let ((candidate)
        (fn (if (eq direction 'forward)
                #'search-forward-regexp
              #'search-backward-regexp)))
    (while (and (not candidate)
                (funcall fn phel-namespace-regexp nil t))
      (let ((end (match-end 0)))
        (save-excursion
          (save-match-data
            (goto-char end)
            (clojure-forward-logical-sexp)
            (unless (or (clojure--in-string-p) (clojure--in-comment-p))
              (setq candidate (string-remove-prefix "'" (thing-at-point 'symbol))))))))
    candidate))

(defun phel-find-ns ()
  ;; FIXME: this doesn't work because \ characters
  ;; are not considered part of symbols.
  (save-excursion
    (save-restriction
      (widen)

      ;; Move to top-level to avoid searching from inside ns
      (ignore-errors (while t (up-list nil t t)))
      
      (or (phel--find-ns-in-direction 'backward)
          (phel--find-ns-in-direction 'forward))))))

(cl-defmethod ilt--find-buffer-module ((ilt-backend (eql 'phel)))
  (phel-find-ns))

(cl-defmethod ilt--init-backend ((_backend (eql 'phel)) _connection)
  )

(defun initialize-ilt-phel-mode ()
  (setq-local ilt-mode-backend-type 'phel))

(add-hook 'phel-mode-hook 'initialize-ilt-phel-mode)
(pushnew 'phel ilt-backends)
(pushnew 'phel-mode ilt--lisp-modes)

(provide 'ilt-phel)

;;; ilt-phel.el ends here
