# DESIGN

Protocol for communicating between Emacs and a live program. 

Generic tools. Specialized backend for each programming language.

## Language candidates

- Python
- Javascript/Node
- Lips scheme. Kawa scheme. Hy lisp. JSCL (Common Lisp in the browser).

## Tools

- Browser tool. 
- XRef.
- Online documentation. Apropos.
- Evaluation.
- Incremental compilation.
- REPL.
- Language specific commands.
- Debugger. Backtrace inspection.
- Inspector.
- Completion.
- Function tracing, breakpoints? (method wrappers)

## Difference with LSP and SLIME/SWANK

SLIME/SWANK design and protocol is oriented towards Lisp languages. This intends to be more generic. Uses S-exp, JSON would be preferred. Lacks browsing capabilities, like returning the members of a package (for browsing).

LSP is complex and does not support incremental evaluation and compilation.

## Protocol

RPC perhaps, or HTTP + JSON.
JSON sounds like a good option since all languages have good support for it.

### Implementation

A protocol server is started on the live program side.

When code is evaluated on the live program side, the result is kept in a list of references. The reference is serialized with id, printed representation, and type.

Backend pseudocode:

```python
results = {};
def evaluate(string):
   res = eval(string)
   id = generate_id()
   results[id] = res
   return JSON.stringify({'id':id, 'type': type(res), 'printed': print(res)})
```
