# Interactive Language Tools

This is a set of tools in Emacs for programming languages that allow interactive programming.

## Motivation

Many languages, specially new and small languages, implement some support for interactive development, but don't come with the tools for realizing that. Examples of that are JSCL (Common Lisp in the web browser), LIPS (a Scheme for the web), .. and many more.

This is my attempt at trying to provide tools for such languages as cheaply as possible.

### Why not Language Server Protocol?

Language Server Protocol is very good, but it doesn't support interactive development. Things like evaluation, REPL, incremental compilation, are not part of the protocol. Also, LSP is very complex to implement. ILT is a minor-mode and can work as a complement of LSP.

### Why not SLIME/SWANK?

SLIME/SWANK are very powerful tools for interactive development in Common Lisp. They are quite specialized towards Lisp, and Lisp like languages. For instance, the protocol is based on S-Expressions. 

ILT is targeted at any kind of language, and implements its protocol via JSONRPC. JSONRPC is probably easier to implement in most languages, as most languages come with very good support of JSON.

Also, ILT tries to be as easy as possible to implement on the targeted programming language side. And uses the target language introspection capabilities to build a semantic model of the elements of the language, so that it can be leveraged by the tools.

## Install

For easy installation, you can add as an Emacs package archive:

```lisp
(add-to-list 'package-archives '("interactive-lang-tools" . "https://codeberg.org/mmontone/interactive-lang-tools/raw/branch/master/archive/"))
```

Then install [the packages](archive/README.md "The list of packages") from `M-x package-list-packages`.

Note that you will need backend files for your programming languages too. You have to download them from this repository.

Load `ilt.el` in Emacs, and the corresponding backend module from `backend` directory.

Then enable `ilt-mode` for your language using hooks:

```lisp
(add-hook 'lisp-mode-hook 'ilt-mode)
```

or call `ilt-mode` interactively in your source buffer. You'll be asked for the backend to use, and the connection options.

## Tools

- [Completion, apropos, eldoc](https://files.gitter.im/5ede953bd73408ce4fe64e26/qOzT/phel2.gif)
- [In buffer evaluation and compilation](https://user-images.githubusercontent.com/436110/186910021-d638eda6-8585-4595-9fa0-1361dadfb30c.mp4)
- [REPL](https://user-images.githubusercontent.com/436110/188273951-c0876f8c-a42c-4764-b41b-216ecf53fc9a.mp4)
- [Inspector](https://user-images.githubusercontent.com/436110/188273951-c0876f8c-a42c-4764-b41b-216ecf53fc9a.mp4)
- [Modules browser](https://user-images.githubusercontent.com/436110/188285959-a77a68bf-be94-4022-92f9-debe39168b8e.mp4)
- Definitions documentation.

## How it works

ILT connects and communicates with the target programming language via a JSONRPC protocol. Sends messages for evaluation, compilation, fetching properties of modules, definitions, etc.

### Protocol

- **evaluate** *(source: string, module-name: string, ctxid: string)*

Evaluates `source` under `module-name` and context with id `ctxid`.

Returns:

```json
{
   "type": "object",
   "properties": {
      "id" : {},
	  "repr" : { "type" : "string"},
	  "type" : { "type" : "string"},
	  "kind" : { "type" : "string"}
   },
   "required": ["repr", "type", "kind"]
}
```

- `id` : When present, a unique identifier referencing the resulting object. That id can be used later by other protocol methods (like `get_definition_properties`) to get information for the referenced objects. Can be anything, decided by the backend. A string, or a more complex object.
- `repr` : The printed representation of the resulting object.
- `type` : The type of the resulting object, in the targeted programming language.
- `kind` : The ILT "type" of the result. One of "boolean", "string", "symbol", "number", "character", "map", "array", "function", "object", "class", "list".

### Semantic model

![semanticmodel](docs/semantic-model.png "Semantic model")

```plantuml
@startuml

class Definition {
   id : object;
   name: string;
   repr: string;
   type: string;
   module: string;
   doc: string;
}

class Module {
  
}

class Function {
   args: Array;
}

class Variable {

}

Module -u-|> Definition
Function -u-|> Definition
Variable -u-|> Definition
Class -u-|> Definition

Module "members *" -> Definition
enum Kind {
   string
   number
   boolean
   object
   class
   map
   array
}

class Reference {
   id: object;
   repr: string;
   type: string;
   module: string;
}

Definition "kind" ->  Kind
Reference "kind" -> Kind
@enduml
```


## Current backends

- [JSCL](backends/jscl "JSCL (Common Lisp in Javascript)")
- [Phel](backends/phel "Phel (Functional PHP language)")
- [Python](backends/python)

## Commands

- `ilt-connect (backend)` - Start an ILT connection with BACKEND language, and bind it to the current buffer.
- `ilt-eval-last-expr` - Evaluate last expression in buffer.
- `ilt-compile-last-expr` - Compile last expression in buffer.

## Connection sessions

Connection sessions are handled via Sesman.
