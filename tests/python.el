(require 'request)
(require 'json)

(setf ilt-connection
      (make-ilt-connection :url "http://localhost:4000/jsonrpc" :backend :python))

(request (ilt-connection-url ilt-connection)
  :type "POST"
  :parser 'json-read
  :data (json-encode
	 '(("method" . "echo")
	   ("params" . ["echome!"])
	   ("jsonrpc" . "2.0")
	   ("id" . 0)))
  :headers '(("Content-Type" . "application/json"))
  :success (cl-function
	    (lambda (&key data &allow-other-keys)
	      (debug data)
	      (message "Res: %s" (assoc-default 'form data)))))

(request (ilt-connection-url ilt-connection)
  :type "POST"
  :parser 'json-read
  :data (json-encode
	 '(("method" . "evaluate")
	   ("params" . ["3 + 3"])
	   ("jsonrpc" . "2.0")
	   ("id" . 0)))
  :headers '(("Content-Type" . "application/json"))
  :success (cl-function
	    (lambda (&key data &allow-other-keys)
	      (let ((res (json-read-from-string (assoc-default 'result data))))
		(message (assoc-default 'repr res))))))
