(cl-defun schema--check-list (schema object &optional error-p)
  (when (not (listp object))
    (error "%s doesn't match %s" object schema))
  (when (not (= (length (cdr schema))
                (length object)))
    (error "%s doesn't match %s" object schema))
  (let ((pairs (cl-mapcar #'cons object (rest schema))))
    (dolist (pair pairs)
      (when (not (schema-check (cdr pair) (car pair)))
        (cl-return-from schema--check-list nil))))
  t)

(cl-defun schema--check-alist (schema object &optional error-p)
  (when (not (listp object))
    (error "%s doesn't match %s" object schema))
  (dolist (assoc-schema (rest schema))
    (let ((key (car assoc-schema))
          (val-type (if (and (consp (cdr assoc-schema))
                             (= (cadr assoc-schema) 32)) ;; optional ? arg
                        (cddr assoc-schema)
                      (cdr assoc-schema)))
          (optional? (and (consp (cdr assoc-schema)) ;; ? character present
                          (= (cadr assoc-schema) 32))))
      (let ((assoc (assoc key object)))
        (if (not assoc)
          (if (not optional?)
              (error "Key missing: %s in %s" key object))
	  (schema-check val-type (cdr assoc))))))
  t)

(defun schema--check-union-type (schema object &optional error-p)
  (dolist (alternative (cdr schema))
    (condition-case nil
	(progn
	  (schema-check alternative object t)
	  (cl-return-from schema--check-union-type t))
      (error nil)))
  nil)

(defun schema-check (schema object &optional error-p)
  (cond
   ((symbolp schema)
    ;; A type specifier
    (or (typep object schema)
        (error "Validation error: %s doesn't match %s" object schema)))
   ;; functions are symbols and `type' invokes predicates, so this is no necessary
   ;; ((functionp schema)
   ;;  (funcall schema object))
   ((listp schema)
    (case (first schema)
      ;;(or (schema--check-union-type schema object error-p))
      ;;(and (schema--check-intersection-type schema object error-p))
      (alist (schema--check-alist schema object error-p))
      (plist (schema--check-plist schema object error-p))
      (list (schema--check-list schema object error-p))
      (t (typep object schema)) ;; a compound type
      ))
   (t (error "Invalid schema: %s" schema))))


;; (schema-check 'string "lala")

;; (schema-check #'stringp "lala")

;; (typep 222 #'integerp)

;; (schema-check '(list integer integer)
;;               '(1 2))

;; (schema-check '(list string integer)
;;               '(1 2))

;; (schema-check '(list integer integer)
;;               '(1 2 3))

;; (schema-check '(alist (:a . string)
;;                       (:b . integer))
;;               '((:a . "hello")
;;                 (:b . 22)))

;; (schema-check '(alist (:a . string)
;;                       (:b . string))
;;               '((:a . "hello")
;;                 (:b . 22)))

;; (schema-check '(alist (:a . string)
;;                       (:b . string))
;;               '((:a . "hello")))

;; (schema-check '(alist (:a . string)
;;                       (:b ? . string))
;;               '((:a . "hello")))

;; (schema-check '(alist (:a ? . string)
;;                       (:b ? . string))
;;               '((:a . "hello")))

;; (schema-check '(alist (:a ? . integer)
;;                       (:b ? . string))
;;               '((:a . "hello")))

;; (schema-check '(or integer string)
;; 	      "lala")

;; (schema-check '(or integer string)
;; 	      22)

(provide 'schema)
