# Python ILT backend

From Emacs: `(require 'ilt-python)`.

Install with `pip3 install .` from `backends/python` directory.

Then use by importing `ilt` module and calling `ilt.start()`:

```python
import ilt
import mylib

if __name__ == '__main__':
    ilt.start()
```

ILT server can also be started in a background thread:

`ilt.start(in_background=True)`

Note that reloading works by reloading whole Python files. The files to reload should be modules for changes to become effective.

So, after making changes to your files, use `M-x ilt-compile-file` command. It reloads the module. Then `ilt-eval` to evaluate, `ilt-apropos` to search for functions, etc.
