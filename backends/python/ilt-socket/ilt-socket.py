# https://jsonrpclib-pelix.readthedocs.io

from jsonrpclib.SimpleJSONRPCServer import SimpleJSONRPCServer
import os
import socket

# Set the path to the socket file
socket_name = "/tmp/ilt.socket"

# Ensure that the file doesn't exist yet (or an error will be raised)
if os.path.exists(socket_name):
   os.remove(socket_name)

try:
   # Start the server, indicating the socket family
   # The server will force some flags when in Unix socket mode
   # (no log request, no reuse address, ...)
   srv = SimpleJSONRPCServer(socket_name, address_family=socket.AF_UNIX)

   # ... register methods to the server

   srv.register_function(lambda x, y: x + y, 'add')
   srv.register_function(lambda x: x, 'ping')

   # Run the server
   srv.serve_forever()
   
except KeyboardInterrupt:
   # Shutdown the server gracefully
   srv.shutdown()
   srv.server_close()
   
finally:
   # You should clean up after the server stopped
   os.remove(socket_name)
