import sys
import types
import traceback
from werkzeug.wrappers import Request, Response
from werkzeug.serving import run_simple
from jsonrpc import JSONRPCResponseManager, dispatcher
import importlib
import threading
import json
import os

references = {}
ids = 0

def generate_id ():
    global ids

    ids = ids + 1
    return ids

def register_reference(obj):
    global references
    
    refid = generate_id()
    references[refid] = obj
    return refid

def get_reference(refid):
    global references
    print('Get reference: ' + str(refid))
    print(references)
    return references[refid]

def object_kind(thing):
    if isinstance(thing, int):
        return 'number'
    if isinstance(thing, float):
        return 'number'
    if isinstance(thing, str):
        return 'string'
    if isinstance(thing, bool):
        return 'boolean'
    if isinstance(thing, dict):
        return 'map'
    if isinstance(thing, list):
        return 'list'
    if isinstance(thing, types.ModuleType):
        return 'module'
    if isinstance(thing, types.FunctionType):
        return 'function'
    if isinstance(thing, types.BuiltinFunctionType):
        return 'function'
    if isinstance(thing, type):
        return 'type'

    return 'object'

def serialize_reference(refid, obj):
    return {
        'id': refid,
        'repr': "{obj}".format(obj=obj),
        'type': '{type}'.format(type=type(obj)),
        'kind' : object_kind(obj) 
    }

def make_reference(obj, **include):
    refid = register_reference(obj)
    ref = serialize_reference(refid, obj)

    if 'doc' in include:
        if hasattr(obj, '__doc__'):
            ref['doc'] = obj.__doc__
        
    return ref

def make_definition(key, thing, module):
    if isinstance(thing, types.ModuleType):
        return {
            'type' : 'module',
            'kind' : 'module',
            'id' : ['module', thing.__name__],
            'name' : thing.__name__,
            'doc' : thing.__doc__,
            'location' : thing.__file__ if '__file__' in dir(thing) else None
        }
    if isinstance(thing, types.FunctionType) or isinstance(thing, types.BuiltinFunctionType):
        return {
            'type' : 'function',
            'kind' : 'function', 
            'id' : ['function', thing.__name__,module.__name__],
            'name' : thing.__name__,
            'module' : module.__name__,
            'doc' : thing.__doc__,
            'location' : [thing.__code__.co_filename, thing.__code__.co_firstlineno] if '__code__' in dir(thing) else None
        }
    if isinstance(thing, type):
        return {
            'type' : 'type',
            'kind' : 'type',
            'id' : ['type', key, module.__name__],
            'name' : thing.__name__,
            'module' : module.__name__,
            'doc': thing.__doc__
        }
    
    # A variable, I assume
    return {
        'type': 'variable',
        'kind': 'variable',
        'id': ['variable', key, module.__name__],
        'module': module.__name__,
        'name' : key
    }

def module_defs(module):
    defs = []
    
    for key in dir(module):
        thing = module.__dict__[key]
        definition = make_definition(key, thing, module)
        defs.append(definition)

    return defs

def module_at_path(filename):
    "Return the module at path if there's any."
    modules = sys.modules.values()
    realpath = os.path.realpath(filename)
    return next((module for module in modules if hasattr(module, '__file__') and module.__file__ == realpath), None)

@dispatcher.add_method
def apropos(string):
    found = []
    added = []
    
    for m_key in sys.modules:
        module = sys.modules[m_key]
        for member_key in dir(module):
            if not member_key in module.__dict__:
                continue
            
            thing = module.__dict__[member_key]

            #if added.__contains__(thing):
            #    continue
            
            added.append(thing)

            if hasattr(thing, '__doc__'):
                thing_doc = thing.__doc__

                if string in member_key:
                    found.append(make_definition(member_key, thing, module))
                    continue

                if isinstance(thing_doc, str):

                    if string in thing_doc:
                        found.append(make_definition(member_key, thing, module))

    return found

@dispatcher.add_method
def list_modules():
    modules = []
    for key in sys.modules:
        module = sys.modules[key]
        if isinstance(module, types.ModuleType):
            modules.append(make_definition(key, module, None))

    return modules

@dispatcher.add_method
def evaluate (str, modulename, ctxid):
    """Evaluate expression in STR.
    MODULE can be a pathname; use module_at_pathname in that case.
    """
    print('Evaluate: {str}'.format(str=str))

    module = None

    if modulename:
        if os.path.isabs(modulename):
            module = module_at_path(modulename)
        else:
            module = sys.modules[modulename]
    
    if module is None:
        module = sys.modules[__name__]

    print('Evaluating in module: {module}'.format(module=module))
        
    res = eval(str, module.__dict__)
    
    print('Evaluated to: {res}'.format(res=res))
    
    refid = register_reference(res)
    serialized_ref = serialize_reference(refid, res)
    
    return serialized_ref

@dispatcher.add_method
def compile(str, modulename, ctxid):
    """Compile expression in STR.
    MODULE can be a pathname; use module_at_pathname in that case.
    """
    
    print('Compile: {str}'.format(str=str))

    module = None

    if os.path.isabs(modulename):
        module = module_at_path(modulename)

    if module is None:
        module = sys.modules[__name__]
    
    res = exec(str, module.__dict__)
    
    return True

@dispatcher.add_method
def load_file(filename):
    """Load filename.
    If filename is a module, then invoke importlib.reload
    """
    module = module_at_path(filename)

    if module is not None:
        print('Reloading module: ' + str(module))
        importlib.reload(module)
    else:
        print('Reading and loading: ' + filename)
        exec(open(filename).read())
        
    return True

@dispatcher.add_method
def compile_file(filename):
    """
    If filename is a module, then invoke importlib.reload
    """
    module = module_at_path(filename)

    if module is not None:
        print('Reloading module: ' + str(module))
        importlib.reload(module)
    else:
        print('Reading and compiling: ' + filename)
        exec(open(filename).read())
        
    return True

@dispatcher.add_method
def reload_module(modulename):
    importlib.reload(sys.modules[modulename])
    return True

## TODO
@dispatcher.add_method
def complete(what):
    return []

## TODO
@dispatcher.add_method
def find_definitions(name, module, deftype):
    return []

def get_module_properties(module, properties):
    props = {
        'name': module.__name__,
        'doc': module.__doc__
    }

    if 'members' in properties:
        members = []
        for key in module.__dir__():
            members.append(make_definition(key, module.__dict__[key], module))
        props['members'] = members

    return props

@dispatcher.add_method
def get_definition_properties(defid, properties):
    deftype = defid[0]
    defname = defid[1]
    
    if deftype == 'module':
        return get_module_properties(sys.modules[defname], properties)
    if deftype == 'type':
        module = sys.modules[defid[2]]
        return get_type_properties(module.__dict__[defname], properties)
    if deftype == 'function':
        module = sys.modules[defid[2]]
        return get_function_properties(module.__dict__[defname], properties)

def get_object_properties(obj, properties):

    props = {}
    
    if isinstance(obj, list):
        props['members'] = [make_reference(x) for x in obj]
        return props
    
    if isinstance(obj, object):
        props['members'] = {k: make_reference(v) for k, v in obj.__dict__.items()}
        return props

@dispatcher.add_method
def get_reference_properties(refid, properties):
    obj = get_reference(refid)
    props = get_object_properties(obj, properties)
    print(props)
    return props
    
@Request.application
def application(request):
    # Dispatcher is dictionary {<method_name>: callable}
    dispatcher["echo"] = lambda s: s
    dispatcher["add"] = lambda a, b: a + b

    response = JSONRPCResponseManager.handle(request.data, dispatcher)
    return Response(response.json, mimetype='application/json')

def start(host='localhost', port=4000, in_background=False):
    """Start an ILT service.
    Use in_background argument to run in a separate thread."""
    
    def start_http_server():
        run_simple(host, port, application)
        
    if not in_background:
        start_http_server()
    else:
        threading.Thread(target=start_http_server).start()

if __name__ == '__main__':
    start()
