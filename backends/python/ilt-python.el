(require 'ilt)
(require 'python)
(require 'json)

(define-ilt-backend python
  (:mode python-mode
	 :lisp-mode nil
	 :mode-default-backend? t))

(cl-defmethod ilt--connect-backend ((_backend (eql 'python)) &rest args)
  "Connect to a Python ILT server."
  (apply #'make-http-ilt-connection
         :url (or (cl-getf args :url)
                  (read-string "Url: "))
         :backend 'python
         args))

(cl-defmethod ilt--find-buffer-module ((_ilt-backend (eql 'python)))
  "Find the Python module for the current buffer."
  ;; Python ILT backend supports sending the current filepath as module,
  ;; instead of the Python module name. The backend is capable of converting
  ;; filepaths to the corresponding module name.
  ;; So, we simply use the buffer file here.
  buffer-file-name)

(defun ilt-python--list-modules-names ()
  (json-parse-string (cdr (assoc 'repr (ilt--eval-sync "json.dumps(list(sys.modules.keys()))")))))

(defun ilt-python-reload-module (modulename)
  "Reload a Python module."
  (interactive (list (completing-read "Reload module:" (ilt-python--list-modules-names))))
  (ilt--eval-sync (format "importlib.reload(sys.modules['%s'])" modulename)))

(provide 'ilt-python)
