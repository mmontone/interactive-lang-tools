#!/usr/bin/env python3

from distutils.core import setup

setup(name='Python ILT backend',
      version='0.1',
      description='Python Interactive Lang Tools backend',
      author='Mariano Montone',
      author_email='marianomontone@gmail.com',
      url='https://www.python.org/sigs/distutils-sig/',
      packages=['ilt'],
     )
