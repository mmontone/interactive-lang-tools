(eval-when (:compile-toplevel :load-toplevel :execute)

  (cl:defpackage :js/syntax
    (:use cl)
    (:export
     :new-with-properties
     :with-object-properties
     :@
     :@{
     :with@
     :->
     :lisp
     :js
     :new))


  (in-package :js/syntax))

;; TODO: consider converting to lisp symbol to json
;; example: :my-symbol -> "mySymbol"
(defun prop-name (thing)
  (cond
    ((stringp thing)
     thing)
    ((keywordp thing)
     (if (string= (symbol-name thing)
                  (string-upcase (symbol-name thing)))
         (string-downcase (symbol-name thing))
         (symbol-name thing)))
    ((symbolp thing)
     (symbol-name thing))
    (t thing)))


(defun new-with-properties (&rest props)
  "Create a new Javascript object with properties.
PROPS is a property list with property names and values.

Example:
(new-with-properties :a 33 :b 40)

it equivalent to the Javascript object:
{a: 33, b: 40}
"
  (let ((obj (jscl::new)))
    (do ((ps props))
        ((zerop (length ps)))
      (let ((key (pop ps))
            (val (pop ps)))
        (let ((key-name (cond
                          ((symbolp key)
                           (string-downcase (symbol-name key)))
                          ((stringp key)
                           key)
                          (t (error "Invalid property name: ~s" key)))))
          (setf (jscl::oget obj key-name) val))))
    obj))

(defun make-object (keys-and-values)
  (let ((obj (jscl::new)))
    (dolist (key-and-value keys-and-values)
      (jscl::oset (cdr key-and-value) obj (string (car key-and-value))))
    obj))

;;(make-object '(("a" . 22) ("b" . t)))


(defmacro with-object-properties (properties js-object &body body)
  "Access properties of JS-OBJECT and bind them in BODY.
Properties can both get accessed and assigned (they are bound using SYMBOL-MACROLET).
PROPERTIES is a list of symbols or (var-name property-name).
The string name of the actual properties is obtained from the symbol name.
So, the property name for a property 'some-property is \"some-property\".
For more control over the property name, use (var-name property-name) as binding.
For example, (some-property \"someProperty\").

Examples:
(with-object-properties (my-prop) some-object
  (print my-prop)
  (setf my-prop 'new-value))

(with-object-properties ((my-prop \"myProp\")) some-object
  (print my-prop))

Also create new objects with properties:
(let ((obj (jscl::new)))
  (with-object-properties (x y) obj
    (setf x 40)
    (setf y 50))
  obj)
"
  (let* ((obj (gensym))
         (bindings (mapcar
                    (lambda (prop)
                      (multiple-value-bind (var-name prop-name)
                          (cond
                            ((symbolp prop)
                             (values prop (string-downcase (symbol-name prop))))
                            ((listp prop)
                             (values (the symbol (car prop)) (the string (cdr prop))))
                            (t (error "Invalid property binding: ~s" prop)))
                        (list var-name `(jscl::oget ,obj ,prop-name))))
                    properties)))
    `(let ((,obj ,js-object))
       (symbol-macrolet ,bindings ,@body))))

;; Syntax

(defun @{ (&rest plist)
  "Create Javascript object from PLIST.

Example:

(@{ :x 433 :y 3434 :|someProp| t)
"
  (let ((obj (jscl::new)))
    (loop for (k v) on plist by #'cddr
          do
             (setf (jscl::oget obj (prop-name k)) v))
    obj))

(defmacro @ (object property)
  "Access OBJECT PROPERTY

Example:

(@ my-object :x)
"
  (list 'jscl::oget object (prop-name property)))

(defmacro with@ (properties object &body body)
  "Bind OBJECT PROPERTIES in BODY."
  `(with-object-properties ,properties ,object ,@body))

(defmacro -> (obj &body clauses)
  "Multi-purpose chain macro.

Useful for calling object methods and working with promises.

Clauses can be:

- A symbol. Then a property is accessed.
- A list starting with a keyword. A method call is performed.
- A list starting with symbol (function name). A chain call is performed.

Example:

;; Call method

(-> obj (:send \"my message\"))
(-> #j:console (:log \"hello\"))

;; Property access
(-> obj :|myAttribute|)

Example:

;; Chain calls

(-> 55
  (+ 55)
  (+ 34 344)
  (- 323 45)
  (:|toString|)
  (:to-string)) ;; TODO

Example:

;; Promises
(-> my-promise
   (:then (lambda (val) (#j:console:log \"Done\" val))))
"
  (let ((accum obj))
    (dolist (clause clauses)
      (cond
	;; Index access
	((numberp clause)
	 (setf accum `(jscl::oget ,accum ,clause)))
        ;; Attribute access
        ((symbolp clause)
         (setf accum `(jscl::oget ,accum ,(prop-name clause))))
        ;; Keyword message
        ((and (listp clause) (keywordp (first clause)))
         (setf accum `((jscl::oget ,accum ,(prop-name (first clause)))
                       ,@(rest clause))))
        ;; Chain call
        ((listp clause)
         (setf accum `(,(first clause) ,accum ,@(rest clause))))
        (t
         (error "Invalid clause: ~s" clause))))
    accum))

;; (-> #j:console (:log "adssf"))
;; (macroexpand '(-> #j:console (:log "lala")))

(defun new (jsclass &rest args)
  "Instantiate a Javascript class."
  (apply #'jscl::make-new jsclass args))

;; Shortcuts for lisp <--> javascript conversions

(defun lisp (thing)
  "Convert THING to Lisp representation."
  (jscl::js-to-lisp thing))

(defun js (thing)
  "Convert THING to Javascript representation."
  (jscl::lisp-to-js thing))
