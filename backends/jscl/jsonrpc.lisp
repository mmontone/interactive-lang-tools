(defpackage :jscl/jsonrpc
  (:use cl js/syntax)
  (:export
   :add-jsonrpc-method
   :jsonrpc-router
   :make-jsonrpc-response
   :make-jsonrpc-error-response
   :handle-jsonrpc-request))

(eval-when (:compile-toplevel :load-toplevel :execute)
  (in-package :jscl/jsonrpc))

(defclass jsonrpc-router ()
  ((methods :accessor jscl/jsonrpc::methods
            :initform (make-hash-table :test #'equal))))

(defun add-jsonrpc-method (router method-name function)
  "Add a method handler to ROUTER."
  (check-type method-name string)
  (setf (gethash method-name (methods router)) function))

(defun handle-jsonrpc-request (router request)
  "Handle REQUEST via ROUTER. Returns a JSONRPC-RESPONSE."
  (with@ (method params id) request
         (let ((method-handler (gethash method (methods router))))
           (when (not method-handler)
             (error "Invalid jsonrpc method: ~s" method))
           (handler-case
               (let ((result (funcall method-handler params)))
                 (make-jsonrpc-response result id))
             (error (e)
               (make-jsonrpc-error-response
                id
                (jscl::error-message e)))))))

;; (js (jscl/jsonrpc::make-jsonrpc-error-response 0 "lala"))

(defun make-jsonrpc-response (result id)
  (@{
   :id id
   :result result
   :jsonrpc "2.0"))

;; (jscl/jsonrpc::make-jsonrpc-response 0 33)

(defun make-jsonrpc-error-response (id error-message &optional data)
  (@{
   :jsonrpc "2.0"
   :id id
   :error (@{
           :code -32000
           :message error-message
           :data (or data (#j:eval "null")))))

;; (jscl/jsonrpc::make-jsonrpc-error-response 0 "some error")
