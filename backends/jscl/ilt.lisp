(eval-when (:compile-toplevel :load-toplevel :execute)

  (defpackage :jscl/ilt
    (:use cl js/syntax jscl/alexandria)
    (:export connect))

  (in-package :jscl/ilt))

(defparameter *websocket* nil)
(defvar *reference-id* 0)
(defvar *references* (make-hash-table))

(defun ws-send (message)
  (-> *websocket* (:send message)))

(defun register-reference (refid obj)
  (setf (gethash refid *references*) obj))

(deftype boolean ()
  `(member t nil))

(defun value-object-p (obj)
  (some (curry #'typep obj)
        '(number string boolean character symbol)))

(defun kind-of (obj)
  (if (eql (type-of obj) 'jscl::js-object)
      (return-from kind-of "object"))
  (condp (curry #'typep obj)
         ('boolean "boolean")
         ('string "string")
         ('symbol "symbol")
         ('number "number")
         ('character "character")
         ('hash-table "map")
         ('vector "array")
         ('function "function")
         ('standard-object "object")
         ('standard-class "class")
         ('cons "list")
         (t (error "What is the kind of:? ~s" obj))))

(defun make-reference (obj &optional ctxid)
  "Make a reference to OBJ in context with CTXID."
  (if (value-object-p obj)
      (@{
       :repr (princ-to-string obj)
       :type (princ-to-string (type-of obj))
       :kind (kind-of obj))
      ;; else
      (let* ((refid (incf *reference-id*))
             (ref
               (@{
                :id refid
                :repr (princ-to-string obj)
                :type (princ-to-string (type-of obj))
                :kind (kind-of obj))))
        (register-reference refid obj)
        ref)))

(defun find-reference (id &optional ctxid)
  (gethash id *references*))

(defun make-definition (symbol type kind &optional ctxid)
  "Creates a reference to a definition.

For the definition ID, ILT allows anything that can be used to uniquely identify the definition.
We use a vector, [cl-type &rest args] for ids."
  (@{
   :id (vector (js (string-downcase (string type)))
               (js (package-name (symbol-package symbol)))
               (js (symbol-name symbol)))
   :name (symbol-name symbol)
   :module (package-name (symbol-package symbol))
   :type (string-downcase (string type))
   :kind kind
   :doc (if (member type '(function variable))
	    (or (documentation symbol type) "")
	    "")))

;;(#j:JSON:stringify (make-definition 'prin1-to-string 'function "function"))

(defun find-definition (id &optional (error-p t))
  "Find definition with by ID.

For the definition ID, ILT allows anything that can be used to uniquely identify the definition.
We use a vector, [kind &rest args] for ids."

  (condp (curry #'string= (aref id 0))
         ("function"
          (let ((fname (intern (aref id 2) (find-package (aref id 1)))))
            (fdefinition fname)))
         ("variable")))

;;(find-definition (vector "CL" "PRIN1-TO-STRING" "function"))

(defun get-definition-properties (id &rest properties)
  "Get PROPERTIES of definition with ID.

For the definition ID, ILT allows anything that can be used to uniquely identify the definition.
We use a vector, [kind &rest args] for ids."
  (let ((def-properties
          (condp (curry #'string= (aref id 0))
                 ("package"
                  (let ((package (find-package (aref id 1)))
                        (defs ()))
                    (do-external-symbols (sym package)
                      (when (fboundp sym)
                        (push (make-definition sym 'function "function") defs))
                      (when (boundp sym)
                        (push (make-definition sym 'variable "variable") defs))
		      (when (jscl::!macro-function sym)
			(push (make-definition sym 'macro "function") defs)))
                    (@{ :id (map 'vector #'js id)
                        :name (aref id 1)
                        :repr (aref id 1)
                        :members (jscl::list-to-vector defs)
                        :doc "" ;; TODO: packages don't have docs in jscl
                        )))
                 ("function"
                  (let ((fname (intern (aref id 2) (find-package (aref id 1)))))
                    (@{ :id (map 'vector #'js id)
                        :name (aref id 2)
                        :repr (aref id 2)
                        :module (aref id 1)
                        :doc (documentation fname 'function))))
                 ("variable"
                  (let ((vname (intern (aref id 2) (find-package (aref id 1)))))
                    (@{ :id (map 'vector #'js id)
                        :name (js (aref id 2))
                        :repr (string (aref id 2))
                        :module (aref id 1)
                        :doc (documentation vname 'variable)))))))
    (when (not def-properties)
      (error "Can't find properties for definition with id: ~s" id))
    def-properties))

;; (#j:JSON:stringify (make-definition 'cl::prin1-to-string 'function "function"))
;; (#j:JSON:stringify (make-definition 'js/syntax::@ 'macro "function"))
;; (#j:JSON:stringify (get-definition-properties (vector "function" "CL" "PRIN1-TO-STRING")))
;; (#j:JSON:stringify (get-definition-properties (vector "module" "JSCL/ALEXANDRIA")))


(defun ilt-get-definition-properties (args)
  (apply #'get-definition-properties
         (map 'vector #'jscl::js-to-lisp (@ args 0))
         (jscl::vector-to-list (@ args 1))))

(defun ilt-evaluate (args)
  (#j:console:log "Evaluate!!:" args)
  (let ((source (@ args 0))
        (package-name (@ args 1))
        (ctxid (@ args 2)))
    (let ((*package* (or (find-package package-name)
                         (find-package :cl-user))))
      (let ((res (eval (read-from-string source))))
        (#j:console:log "Evaluated to: " res)
        (make-reference res ctxid)))))

(defun ilt-apropos (args)
  (#j:console:log "Apropos!: " args)
  (let* ((string (@ args 0))
         (found (apropos-list (string-upcase string)))
         defs)

    (dolist (symbol found)
      (when (boundp symbol)
        (push (make-definition symbol 'variable "variable")
              defs))
      (when (fboundp symbol)
        (push (make-definition symbol 'function "function")
              defs)))

    (apply #'vector defs)))

(defun complete-symbol (prefix-string)
  (let (completions
        (prefix-len (length prefix-string)))
    (do-symbols (sym *package*)
      (let ((sym-name (symbol-name sym)))
        (when (and (>= (length sym-name) prefix-len)
                   (string= (string-upcase prefix-string)
                            (subseq sym-name 0 (length prefix-string))))
          (push sym completions))))
    completions))

;;(jscl/ilt::complete-symbol "eva")
;;(complete-symbol "LISP")

(defun ilt-complete (args)
  (let* ((prefix (@ args 0))
         (completions (complete-symbol prefix)))
    (apply #'vector (mapcar (lambda (comp)
                              (@{ :completion (string comp)))
                            completions))))

(defun ilt-list-modules (args)
  (apply #'vector (mapcar (lambda (package)
                            (@{
                             :id  (vector (js "package")
                                          (js (package-name package)))
                             :name (package-name package)
                             :type "package"
                             :kind "module"
                             :doc "" ;; jscl doesn't support package docstrings
                             ))
                          (cl::list-all-packages))))

(defgeneric get-object-properties (object &rest properties))

(defmethod get-object-properties ((object list) &rest properties)
  (@{
   :repr (princ-to-string object)
   :members (apply #'vector (mapcar #'make-reference object))
   :length (length object)))

;; (#j:JSON:stringify (get-object-properties (list 1 2 3)))

(defmethod get-object-properties ((object hash-table) &rest properties)
  (@{
   :repr (princ-to-string object)
   :members (apply #'vector
                   (mapcar (lambda (cons) (vector (make-reference (car cons))
                                             (make-reference (cdr cons))))
                           (hash-table-alist object)))
   :count (hash-table-count object)))

#+nil(#j:JSON:stringify (get-object-properties
                         (let ((tab (make-hash-table)))
                           (setf (gethash :a tab) "foo")
                           (setf (gethash "lalal" tab) (list 1 2 3))
                           tab)))


(defmethod get-object-properties ((object jscl::js-object) &rest properties)
  (let* ((keys (mapcar #'jscl::js-to-lisp (jscl::vector-to-list (#j:Object:keys object))))
         (members (js/syntax::make-object (mapcar (lambda (key)
                                                    (cons (string key)
                                                          (make-reference (@ object (string key)))))
                                                  keys))))
    (@{
     :repr (princ-to-string object)
     :members members)))

#+nil(#j:JSON:stringify (get-object-properties #j:window:document))
#+nil(#j:JSON:stringify (get-object-properties #j:window:jscl))

(defmethod get-object-properties ((object standard-object) &rest properties)
  (let* ((slot-names
           (remove-duplicates
            (mapcar (rcurry #'getf :name)
                    (apply #'append
                           (mapcar #'jscl::class-slots
                                   (jscl::class-precedence-list (class-of object)))))))
         (members (js/syntax::make-object
                   (mapcar (lambda (slot-name)
                             (cons (prin1-to-string slot-name)
                                   (make-reference (slot-value object slot-name))))
                           slot-names))))
    (@{
     :repr (princ-to-string object)
     :members members)))

#+nil(#j:JSON:stringify (get-object-properties *router*))

(defun ilt-get-reference-properties (args)
  (let ((ref (find-reference (@ args 0))))
    (when ref
      (apply #'get-object-properties ref (jscl::vector-to-list (@ args 1))))))

(defun ilt-find-definitions (args)
  (let ((def-name (@ args 0))
        (package-name (@ args 1))
        defs)
    (let ((symbol (if (find #\: def-name)
		      ;; a qualified symbol
		      (read-from-string (string-upcase def-name))
		      ;; else
		      (intern (string-upcase def-name)
                              (or (find-package package-name)
				  *package*)))))
      (when (fboundp symbol)
        (push (make-definition symbol 'function "function") defs))
      (when (boundp symbol)
        (push (make-definition symbol 'variable "variable") defs))
      (when (jscl::!macro-function symbol)
	(push (make-definition symbol 'macro "function") defs))
      (when (find-class symbol nil)
        (push (make-definition symbol 'class "type") defs)))
    (apply #'vector defs)))

;; (#j:JSON:stringify (ilt-find-definitions (vector "*router*" "JSCL/ILT")))

(defun ilt-load-file (args)
  (error "Cannot access files"))

(defun ilt-load-source (args)
  "Evaluate top level forms in source."
  (let ((source (first args)))
    (with-input-from-string (s source)
      (loop for expr := (read s nil :eof)
            while (not (eql expr :eof))
            do (eval read))))
  t)

(defvar *router* (make-instance 'jscl/jsonrpc:jsonrpc-router)
  "The JSCL JSONRPC backend router.")

(jscl/jsonrpc:add-jsonrpc-method *router* "evaluate" 'ilt-evaluate)
(jscl/jsonrpc:add-jsonrpc-method *router* "apropos" 'ilt-apropos)
(jscl/jsonrpc:add-jsonrpc-method *router* "complete" 'ilt-complete)
(jscl/jsonrpc:add-jsonrpc-method *router* "list_modules" 'ilt-list-modules)
(jscl/jsonrpc:add-jsonrpc-method *router* "load_file" 'ilt-load-file)
(jscl/jsonrpc:add-jsonrpc-method *router* "load_source" 'ilt-load-source)
(jscl/jsonrpc:add-jsonrpc-method *router* "compile" 'ilt-evaluate)
(jscl/jsonrpc:add-jsonrpc-method *router* "get_reference_properties" 'ilt-get-reference-properties)
(jscl/jsonrpc:add-jsonrpc-method *router* "get_definition_properties" 'ilt-get-definition-properties)
(jscl/jsonrpc:add-jsonrpc-method *router* "find_definitions" 'ilt-find-definitions)

(defun connect (&optional (port 3000))
  (#j:console:log "Connecting...")

  (setf *websocket*
        (jscl::make-new #j:WebSocket (js (format nil "ws://localhost:~a" port))))

  (#j:console:log "Connected:" *websocket*)

  (setf (@ *websocket* :|onmessage|)
        (lambda (ev)
          (#j:console:log ev)
          (#j:console:log "Requested:" (#j:JSON:parse (@ ev "data")))
          (handler-case
              (let ((res (jscl/jsonrpc:handle-jsonrpc-request
                          *router*
                          (jscl::js-to-lisp
                           (#j:JSON:parse (@ ev "data"))))))

                (#j:console:log "Response: " res
                                (#j:JSON:stringify res))
                (jscl/ilt::ws-send (#j:JSON:stringify res)))
            (error (e)
              (#j:console:error "JSONRPC error: " e (jscl::error-message e))
              (ws-send (jscl::error-message e))))))
  t)

;; Connect on load

(in-package :cl-user)

(eval-when (:execute)
  (jscl/ilt::connect))
