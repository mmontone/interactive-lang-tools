;;; ilt-jscl.el --- Interactive Language Tools backend for JSCL  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Version: 0.1
;; Package-Requires: ((emacs "25") (ilt "0.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Interactive Language Tools backend for JSCL

;;; Code:

(require 'ilt)

(cl-defmethod ilt--connect-backend ((backend (eql 'jscl)) &rest args)
  (apply #'make-ilt--websocket-connection
	 :port (or (cl-getf args :port)
		   (string-to-number (read-string "Port: ")))
	 :backend 'jscl
	 args))

(cl-defmethod ilt--find-buffer-module ((ilt-backend (eql 'jscl)))
  (let ((case-fold-search t)
        (regexp (concat "^[ \t]*(\\(cl:\\|common-lisp:\\)?in-package\\>[ \t']*"
                        "\\([^)]+\\)[ \t]*)")))
    (save-excursion
      (when (or (re-search-backward regexp nil t)
                (re-search-forward regexp nil t))
        (upcase (cl-subseq (match-string-no-properties 2) 1))))))

(cl-defmethod ilt--init-backend ((_backend (eql 'jscl)) _connection))

(pushnew 'jscl ilt-backends)

(provide 'ilt-jscl)

;;; ilt-jscl.el ends here
