;; Load this file

(defpackage :ilt/jscl
  (:use :cl)
  (:export :bundle-ilt-application))

(in-package :ilt/jscl)

(defparameter *source-directory* (UIOP:pathname-directory-pathname *load-pathname*))

(defun source-path (filename)
  (merge-pathnames filename *source-directory*))

(defun bundle-application (files output-pathname &key verbose)
  (let ((*features* (list* :jscl :jscl-xc *features*))
        (*package* (find-package "JSCL"))
        (*default-pathname-defaults* jscl::*base-directory*))
    (setq jscl::*environment* (jscl::make-lexenv))
    (jscl::with-compilation-environment
      (with-open-file (out output-pathname
                           :direction :output
                           :if-exists :supersede)
        (format out "(function(){~%")
        (format out "'use strict';~%")
        (write-string (jscl::read-whole-file (jscl::source-pathname "prelude.js")) out)
        (jscl::do-source input :target
          (jscl::!compile-file input out :print verbose))

	;; NOTE: This file must be compiled after the global
        ;; environment. Because some web worker code may do some
        ;; blocking, like starting a REPL, we need to ensure that
        ;; *environment* and other critical special variables are
        ;; initialized before we do this.
        (jscl::!compile-file "src/toplevel.lisp" out :print verbose)

	;; Compile application files

	(dolist (file files)
	  (jscl::!compile-file file out :print nil))
	
	(write-string (jscl::compile-toplevel '(cl:in-package :cl-user)))
	
        (jscl::dump-global-environment out)        
        
        (format out "})();~%")))

    (jscl::report-undefined-functions)))

(defun bundle-ilt-application ()
  (bundle-application 
   (mapcar #'source-path '("js-syntax.lisp"
			   "alexandria.lisp"
			   "jsonrpc.lisp"
			   "ilt.lisp" ))
   #p"/home/marian/src/lisp/jscl/jscl.js"))
