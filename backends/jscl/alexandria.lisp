;; TODO: just port ALEXANDRIA ??

(defpackage :jscl/alexandria
  (:use :cl)
  (:export
   :condp
   :curry
   :rcurry
   :hash-table-keys
   :hash-table-values
   :hash-table-alist
   :compose
   :flatten))

(in-package :jscl/alexandria)

(defun jscl::error-message (err)
  (if (typep err 'simple-condition)
      (apply #'format nil
             (simple-condition-format-control err)
             (simple-condition-format-arguments err))
      (princ-to-string err)))

(defmacro condp (predicate &body clauses)
  "COND using PREDICATE."
  (let ((pred (gensym)))
    `(let ((,pred ,predicate))
       (cond
         ,@(loop for clause in clauses
                 collect `((funcall ,pred ,(car clause))
                           ,@(cdr clause)))))))

(defun curry (func &rest args)
  (lambda (&rest argss)
    (apply func (append args argss))))

(defun rcurry (func &rest args)
  (lambda (&rest argss)
    (apply func (append argss args))))

(defun ensure-function (function-designator)
  "Returns the function designated by FUNCTION-DESIGNATOR:
if FUNCTION-DESIGNATOR is a function, it is returned, otherwise
it must be a function name and its FDEFINITION is returned."
  (if (functionp function-designator)
      function-designator
      (fdefinition function-designator)))

(defun compose (function &rest more-functions)
  "Returns a function composed of FUNCTION and MORE-FUNCTIONS that applies its
arguments to to each in turn, starting from the rightmost of MORE-FUNCTIONS,
and then calling the next one with the primary value of the last."
  (reduce (lambda (f g)
	    (let ((f (ensure-function f))
		  (g (ensure-function g)))
	      (lambda (&rest arguments)
		(declare (dynamic-extent arguments))
		(funcall f (apply g arguments)))))
          more-functions
          :initial-value function))

(defun hash-table-keys (table)
  (let ((keys nil))
    (maphash (lambda (k v)
	       (declare (ignore v))
	       (push k keys))
	     table)
    keys))

(defun hash-table-values (table)
  (let ((values nil))
    (maphash (lambda (k v)
	       (declare (ignore k))
	       (push v values))
	     table)
    values))

(defun hash-table-alist (table)
  "Returns an association list containing the keys and values of hash table
TABLE."
  (let ((alist nil))
    (maphash (lambda (k v)
               (push (cons k v) alist))
             table)
    alist))

(defun flatten (tree)
  "Traverses the tree in order, collecting non-null leaves into a list."
  (let (list)
    (labels ((traverse (subtree)
               (when subtree
                 (if (consp subtree)
                     (progn
                       (traverse (car subtree))
                       (traverse (cdr subtree)))
                     (push subtree list)))))
      (traverse tree))
    (nreverse list)))
