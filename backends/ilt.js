const express = require("express");
const bodyParser = require("body-parser");
const { JSONRPCServer } = require("json-rpc-2.0");

const server = new JSONRPCServer();

// First parameter is a method name.
// Second parameter is a method itself.
// A method takes JSON-RPC params and returns a result.
// It can also return a promise of the result.
server.addMethod("echo", ({ text }) => text);
server.addMethod("log", ({ message }) => console.log(message));

const references = {};
var refs_id = 0;

function gen_id() {
    return refs_id++;
}

function get_reference(id) {
    return references[id];
}

function register_reference(obj) {
    let ref_id = gen_id();
    references[ref_id] = obj;
    return ref_id;
}

function serialize_reference(id, obj) {
    return {
	'id': id,
	'repr': String(obj),
	'type': typeof obj
    };
}

function evaluate (source) {
    console.log('Evaluate: ', source[0]);
    let res = eval(source[0]);
    console.log('Evaluated: ', res);
    let ref_id = register_reference(res);
    return JSON.stringify(serialize_reference(ref_id, res));
}

server.addMethod("evaluate", evaluate);

const app = express();
app.use(bodyParser.json());

app.post("/jsonrpc", (req, res) => {
  const jsonRPCRequest = req.body;
  // server.receive takes a JSON-RPC request and returns a promise of a JSON-RPC response.
  // It can also receive an array of requests, in which case it may return an array of responses.
  // Alternatively, you can use server.receiveJSON, which takes JSON string as is (in this case req.body).
  server.receive(jsonRPCRequest).then((jsonRPCResponse) => {
    if (jsonRPCResponse) {
      res.json(jsonRPCResponse);
    } else {
      // If response is absent, it was a JSON-RPC notification method.
      // Respond with no content status (204).
      res.sendStatus(204);
    }
  });
});

app.listen(4000);
