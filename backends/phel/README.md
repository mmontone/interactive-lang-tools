# Phel ILT backend

## Install and setup

* Install ILT packages: ilt, ilt-phel, phel-mode

* In your .emacs:

    ```lisp
    (require 'phel-mode)
    (require 'ilt-phel)

    (add-to-list 'auto-mode-alist '("\\.phel\\'" . phel-mode))

    (add-hook 'phel-mode-hook 'ilt-mode)
    ```

* Run the Phel ILT server

    I would like to make this step better, but I don't know how. (*IF YOU HAVE IDEAS ON HOW TO IMPROVE THIS STEP, PLEASE LET ME KNOW!*)

    Copy `ilt.phel` to your project and start it:
    `~/src/phel-scaffolding $ php -S localhost:8080 -t ./src/web`

* Open your Phel files and connect.

    When asked about the connection url, enter the url with `/ilt` handler. For example: `http://localhost:8080/ilt`.

## Usage

Syntax highlighting, indentation, completion, eldoc, are supported, with limitations.

Commands:
* `C-x C-e` (or `M-x ilt-eval-last-expr`) to evaluate Phel code interactively from your buffer.
* `M-x phel-repl` to start a REPL.

## Screenshots

![phel1](https://files.gitter.im/5ede953bd73408ce4fe64e26/GL5B/phel.gif)
![phel2](https://files.gitter.im/5ede953bd73408ce4fe64e26/qOzT/phel2.gif)
![phel-docs](https://files.gitter.im/5ede953bd73408ce4fe64e26/COD6/phel-docs.gif)
