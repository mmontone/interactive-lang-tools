;;; ilt-phel.el --- Interactive Language Tools backend for Phel language  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Version: 0.2
;; Package-Requires: ((emacs "25") (ilt "0.1") (phel-mode "0.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Interactive Language Tools backend for Phel language

;;; Code:

(require 'ilt)
(require 'phel-mode)

(cl-defmethod ilt--connect-backend ((_backend (eql 'phel)) &rest args)
  "Connect to a Phel backend."
  (apply #'make-http-ilt-connection
         :url (or (cl-getf args :url)
                  (read-string "Url: "))
         :backend 'phel
         args))

(defconst phel-namespace-regexp
  (rx line-start "(" (? "phel.core/") (or "in-ns" "ns" "ns+") symbol-end))

(defun phel--find-ns-in-direction (direction)
  "Return the nearest namespace in a specific DIRECTION.
DIRECTION is `forward' or `backward'."
  (let ((candidate)
        (fn (if (eq direction 'forward)
                #'search-forward-regexp
              #'search-backward-regexp)))
    (while (and (not candidate)
                (funcall fn phel-namespace-regexp nil t))
      (let ((end (match-end 0)))
        (save-excursion
          (save-match-data
            (goto-char end)
            (clojure-forward-logical-sexp)
            (unless (or (clojure--in-string-p) (clojure--in-comment-p))
              (setq candidate (string-remove-prefix "'" (thing-at-point 'symbol))))))))
    candidate))

(defun phel-find-ns ()
  ;; FIXME: this doesn't work because \ characters
  ;; are not considered part of symbols.
  (save-excursion
    (save-restriction
      (widen)

      ;; Move to top-level to avoid searching from inside ns
      (ignore-errors (while t (up-list nil t t)))

      (or (phel--find-ns-in-direction 'backward)
          (phel--find-ns-in-direction 'forward)))))

(cl-defmethod ilt--find-buffer-module ((_ilt-backend (eql 'phel)))
  (phel-find-ns))

;; Phel is a bit slow, and completions take too much time.
;; Provide the option to use precalculated completions.

(cl-defun ilt-phel--pre-completion-at-point ()
  "Complete thing at point.
Calls the `complete' jsonrpc method.

Returns (START END COLLECTION . PROPS), where:
 START and END delimit the entity to complete and should include point,
 COLLECTION is the completion table to use to complete the entity, and
 PROPS is a property list for additional information."
  (cl-destructuring-bind (start end what-to-complete)
      (ilt--what-to-complete (ilt-connection-backend (ilt-connection)))
    ;; Return the completions table
    ;; Note that we don't filter the completion tables using `start' and `end',
    ;; we only return the appropiate table. The `completion-styles' are in charge
    ;; of the filtering.
    ;; See `completion-at-point-functions' docs for more info.
    (list start end
	  (cond
	   ((or (not start) (not end)) nil)
	   ((s-starts-with-p "php/" what-to-complete)
	    (mapcar (lambda (name) (concat "php/" (symbol-name name))) ilt-phel--php-defs))
	   (t ilt-phel--phel-defs)))))
  
(defvar ilt-phel--phel-defs nil)
(defvar ilt-phel--php-defs nil)

(defun phel-precalculate-completions ()
  "Enable completion precalculation for phel."
  (interactive)
  (setq ilt-phel--phel-defs
	(car
	 (read-from-string
	  (assoc-default 'repr
			 (ilt--eval-sync
			  "(all-phel-def-names)")))))
  (setq ilt-phel--php-defs
	(car
	 (read-from-string
	  (assoc-default 'repr
			 (ilt--eval-sync
			  "(all-php-def-names)")))))
  ;; (add-hook 'completion-at-point-functions
  ;;          #'ilt-phel-pre-completion-at-point -10 'local)
  (setq-local completion-at-point-functions
	      (list 'ilt-phel--pre-completion-at-point))
  )

(define-ilt-backend phel
  (:mode phel-mode
	 :lisp-mode t
	 :mode-default-backend? t))

(provide 'ilt-phel)

;;; ilt-phel.el ends here
