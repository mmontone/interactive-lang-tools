;;; phel-mode.el --- Major mode for editing Phel language source files  -*- lexical-binding: t; -*-

;; Copyright (C) 2022  Mariano Montone

;; Author: Mariano Montone <marianomontone@gmail.com>
;; Version: 0.2
;; Keywords: tools, editing, PHP, phel
;; Package-Requires: ((emacs "25") (clojure-mode "5.15.1"))

;; This program is free software; you can redistribute it and/or modify
;; it under the terms of the GNU General Public License as published by
;; the Free Software Foundation, either version 3 of the License, or
;; (at your option) any later version.

;; This program is distributed in the hope that it will be useful,
;; but WITHOUT ANY WARRANTY; without even the implied warranty of
;; MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;; GNU General Public License for more details.

;; You should have received a copy of the GNU General Public License
;; along with this program.  If not, see <https://www.gnu.org/licenses/>.

;;; Commentary:

;; Major mode for editing Phel language source files.

;;; Code:

(require 'clojure-mode)

(define-derived-mode phel-mode clojure-mode "Phel"
  "Major mode for editing Phel language source files."
  (setq-local comment-start "#")
  ;; We disable lockfiles so that ILT evaluation works.
  ;; The lockfiles seem to modify the buffer-file-name somehow, when the buffer changes
  ;; And that is detected by the currently running Phel process.
  ;; That interferes with evaluation, as the running Phel process starts behaving badly because of that.
  (setq-local create-lockfiles nil)
  ;;(setq-local company-backends '((company-capf company-dabbrev-code)))
  )

(provide 'phel-mode)
;;; phel-mode.el ends here
