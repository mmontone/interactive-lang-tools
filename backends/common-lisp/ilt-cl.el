(require 'ilt)

(cl-defmethod ilt--init-backend ((backend (eql 'common-lisp)) connection)
  (ignore backend))

(cl-defmethod ilt--find-buffer-module ((ilt-backend (eql 'common-lisp)))
  (let ((case-fold-search t)
        (regexp (concat "^[ \t]*(\\(cl:\\|common-lisp:\\)?in-package\\>[ \t']*"
                        "\\([^)]+\\)[ \t]*)")))
    (save-excursion
      (when (or (re-search-backward regexp nil t)
                (re-search-forward regexp nil t))
        (match-string-no-properties 2)))))

(pushnew 'common-lisp ilt-backends)

(provide 'ilt-cl)
