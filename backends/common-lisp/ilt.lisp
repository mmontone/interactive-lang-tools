;; Common Lisp backend

(require :jsonrpc)
(require :def-properties)
(require :cl-json)
(require :hunchentoot)

(defpackage :interactive-language-tools
  (:nicknames :ilt)
  (:use :cl)
  (:export :start))

(in-package :ilt)

(defparameter *references* (make-hash-table))

(defparameter *ref-id* 0)

(defmacro condp (predicate &body clauses)
  "COND using PREDICATE."
  (let ((pred (gensym)))
    `(let ((,pred ,predicate))
       (cond
         ,@(loop for clause in clauses
                 collect `((funcall ,pred ,(car clause))
                           ,@(cdr clause)))))))

(defun kind-of (obj)
  (condp (alexandria:curry #'typep obj)
    ('number "number")
    ('string "string")
    ('boolean "boolean")
    ('hash-table "map")
    ('list "list")
    ('vector "array")
    ('function "function")
    ('package "module")
    ('class "type")
    (t "unknown")))

(defun find-reference (id &optional (error-p t))
  (or (gethash id *references*)
      (and error-p
           (error "Reference with id: ~a not found." id))))

(defun generate-ref-id ()
  (incf *ref-id*))

(defun register-reference (obj)
  (let ((ref-id (generate-ref-id)))
    (setf (gethash ref-id *references*) obj)
    ref-id))

(defun serialize-reference (id obj)
  (list
   (cons :id id)
   (cons :name (princ-to-string obj))
   (cons :repr (princ-to-string obj))
   (cons :type (princ-to-string (type-of obj)))
   (cons :kind (kind-of obj))))

(defun make-reference (obj &rest include)
  (let* ((ref-id (register-reference obj))
         (ref (serialize-reference ref-id obj)))
    (when (member :doc include)
      (push (cons :doc (documentation obj t)) ref))
    (alexandria:alist-hash-table ref)))

(defvar *server*
  (jsonrpc:make-server))

(defun ilt-apropos-symbol (string)
  (let (found)
    (dolist (package (remove "KEYWORD" (list-all-packages)
                             :key #'package-name
                             :test #'string=))
      (do-external-symbols (sym package)
        (when (search (string-upcase string) (symbol-name sym) :test #'string=)
          (push sym found))))
    found))

(defmacro cond* (&body clauses)
  "Like COND, but applies all CLAUSES that match."
  `(progn
     ,@(loop for clause in clauses
             collect `(when ,(car clause)
                        ,@(cdr clause)))))

(defun ilt-apropos-grouped (string)
  (let ((symbols (ilt-apropos-symbol string)))
    (let (functions variables classes macros types)
      (dolist (sym symbols)
        (cond*
          ((macro-function sym) (push sym macros))
          ((find-class sym nil) (push sym classes))
          ((boundp sym) (push sym variables))
          ((fboundp sym) (push sym functions))))
      (list (cons 'cl:function functions)
            (cons 'cl:variable variables)
            (cons 'cl:class classes)
            (cons 'cl:macro-function macros)
            (cons 'cl:type types)))))

(defun ilt-apropos-with-type (string)
  (let ((symbols (ilt-apropos-symbol string)))
    (let (functions variables classes macros types)
      (dolist (sym symbols)
        (cond*
          ((macro-function sym) (push sym macros))
          ((find-class sym nil) (push sym classes))
          ((boundp sym) (push sym variables))
          ((fboundp sym) (push sym functions))))
      (append
       (mapcar (alexandria:curry #'cons 'cl:function)
               functions)
       (mapcar (alexandria:curry #'cons 'cl:variable)
               variables)
       (mapcar (alexandria:curry #'cons 'cl:class)
               classes)
       (mapcar (alexandria:curry #'cons 'cl:macro-function)
               macros)
       (mapcar (alexandria:curry #'cons 'cl:type)
               types)))))

(ilt-apropos-with-type "list")

(defun make-definition (type symbol)

  (when (eq type :package)
    (let ((package (find-package symbol)))
      (return-from make-definition
        (alexandria:alist-hash-table
         (list
          (cons "id" (package-name package))
          (cons "name" (package-name package))
          (cons "doc" (documentation package t))
          (cons "type" "package")
          (cons "kind" "module"))))))

  (let* ((kind (ecase type
                 (cl:variable "variable")
                 (cl:macro-function "function")
                 (cl:function "function")
                 (cl:class "type")
                 (cl:type "type")))
         (props
           (first (def-properties:symbol-properties symbol t type))))
    (alexandria:alist-hash-table
     (list (cons "id" (format nil "~a::~a" (package-name (symbol-package symbol))
                              (symbol-name symbol)))
           (cons "module" (princ-to-string (package-name (symbol-package symbol))))
           (cons "name" (princ-to-string symbol))
           (cons "doc" (alexandria:assoc-value props :documentation))
           (cons "type" (string-downcase (symbol-name type)))
           (cons "kind" (string-downcase (princ-to-string kind)))))))

(defun ilt-apropos (args)
  (let ((matches (ilt-apropos-with-type (first args))))
    (loop for (type . symbol) in matches
          collect (make-definition type symbol))))

(defun ilt-evaluate (args)
  (let ((*package* (find-package (or (second args) "CL"))))
    (let ((form (read-from-string (first args))))
      (let ((result (eval form)))
        (make-reference result)))))

(defun ilt-get-reference-properties (args)
  (destructuring-bind (ref-id &optional properties) args
    (declare (ignore properties))
    (let ((ref (find-reference ref-id))
          (props '()))
      (when (listp ref)
        (push (cons :members (mapcar #'make-reference ref))
              props)
        (push (cons :length (length ref)) props))
      (alexandria:alist-hash-table props))))

(defun ilt-list-modules (args)
  (declare (ignore args))
  (mapcar (alexandria:curry #'make-definition :package)
          (list-all-packages)))

(jsonrpc:expose *server* "apropos" 'ilt-apropos)

(defun start-jsonrpc (&key (port 4000) (mode :tcp))
  (jsonrpc:server-listen *server* :port port :mode mode)
  *server*)

(defvar *acceptor*)

(defvar *jsonrpc-methods* (make-hash-table :test 'equalp))

(setf (gethash "apropos" *jsonrpc-methods*) 'ilt-apropos)
(setf (gethash "evaluate" *jsonrpc-methods*) 'ilt-evaluate)
(setf (gethash "get_reference_properties" *jsonrpc-methods*) 'ilt-get-reference-properties)
(setf (gethash "get_definition_properties" *jsonrpc-methods*) 'ilt-get-reference-properties)
(setf (gethash "list_modules" *jsonrpc-methods*) 'ilt-list-modules)

(defun start (&key (port 4000))
  (setf *acceptor* (hunchentoot:start (make-instance 'hunchentoot:easy-acceptor :port port))))

(defun parse-jsonrpc-message (message)
  (json:decode-json-from-string message))

(defun encode-jsonrpc-response (result message)
  (json:encode-json-alist-to-string
   (list (cons :id (alexandria:assoc-value message :id))
         (cons :result result)
         (cons :jsonrpc "2.0"))))

(defvar *jsonrpc-error-codes*
  '((-32700  "Parse error"  "Invalid JSON was received by the server.
      An error occurred on the server while
      parsing the JSON text.")
    (-32600  "Invalid Request"  "The JSON sent is not a valid Request object. ")
    (-32601  "Method not found"  "The method does not exist / is not
      available.")
    (-32602  "Invalid params"  "Invalid method parameter(s).")
    (-32603  "Internal error"  "Internal JSON-RPC error.")
    (-32000 to -32099  "Server error"  "Reserved for implementation-defined
      server-errors.")))

(defun simple-condition-message (simple-condition)
  (apply #'format nil
         (simple-condition-format-control simple-condition)
         (simple-condition-format-arguments simple-condition)))

(defun encode-jsonrpc-error-response (error message)
  (json:encode-json-alist-to-string
   (list (cons :id (when message (alexandria:assoc-value message :id)))
         (cons :error (list (cons :code (if message -32603 -32600))
                            (cons :message (with-standard-io-syntax (princ-to-string error)))))
         (cons :jsonrpc "2.0"))))

(hunchentoot:define-easy-handler (receive-jsonrpc-message :uri "/")
    ()
  (handler-case
      (let ((message (parse-jsonrpc-message
                      (hunchentoot:raw-post-data :force-text t))))
        (handler-case
            (let ((result
                    (funcall (gethash (alexandria:assoc-value message :method) *jsonrpc-methods*)
                             (alexandria:assoc-value message :params))))
              (encode-jsonrpc-response result message))
          (error (e)
            (encode-jsonrpc-error-response e message))))
    (error (e)
      (encode-jsonrpc-error-response e nil))))

(provide 'ilt)
